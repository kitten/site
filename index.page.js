import kitten from '@small-web/kitten'

import MainLayout from './Main.layout.js'
import Platform from './Platform.script.js'
import InstallationInstructions from './InstallationInstructions.fragment.js'
import { formatRelative } from 'date-fns'

let version
const getVersion = async () => {
  let response
  try {
    response = await fetch('https://kittens.small-web.org/latest/')
    if (response.ok) {
      const _version = await response.json()
      const link = `https://codeberg.org/kitten/app/commit/${_version.latest.gitHash}`
      const releaseDate = _version.latest._releaseDate
      const date = formatRelative(new Date(releaseDate), new Date())
      version = {
        date,
        link
      }
    } else {
      console.warn(`Latest version information fetch response from kittens.small-web.org not OK.Code:`, response.status) 
      return
    }
  } catch (error) {
    console.warn(`Latest version information fetch attempt failed.`, error)
    version = {
      date: ['<strong class="prereleaseMessage">unknown (are you offline?)</strong>'],
      link: ''
    }
  }
}
await getVersion()

// Get the latest data every 30 seconds.
setInterval(getVersion, 30000)

const Toast = ({SLOT}) => kitten.html`
  <div id='toast' morph='settle:2s'>
    ${SLOT}
  </div>
`

export function onConnect ({ page }) {
  // When the platform tab is changed.
  page.on('platform', data => {
    page.send(kitten.html`
      <${InstallationInstructions} platform=${Platform.withValue(data.value)} version=${version} />
    `)
  })

  // When a code copy button is pressed.
  page.on('copy', data => {
    const modifierKey = data.platform === Platform.MAC ? 'Command' : 'Ctrl'
    page.send(kitten.html`<${Toast}>Copied! Use <kbd>${modifierKey}</kbd><kbd>v</kbd> to paste.</>`)
  })
}


export default ({ request }) => {
  const platform = Platform.fromRequest(request)

  return kitten.html`
    <${MainLayout} pageId='home'>
      <${Toast} />
      <style>
        #toast.settling { opacity: 1; top: 1em; }
        #toast {
          opacity: 0;
          top: -3em;
          position: fixed;
          transition: all 0.3s ease-out;
          right: 1em;
          border-radius: 1em;
          background-color: var(--focus);
          color: var(--background-body);
          padding: 1em;
          font-weight: bold;
        }
      </style>
      <div class='subtitle'>
        <markdown>
          A 💕 [Small Web](https://ar.al/2020/08/07/what-is-the-small-web/) development kit.
        </markdown>
      </div>
      <markdown>
        - Build using HTML, CSS, and JavaScript.
        - Progressively enhance with [Streaming HTML](/tutorials/streaming-html) and htmx.
        - Go beyond traditional web apps to create peer-to-peer 💕 [Small Web](https://ar.al/2020/08/07/what-is-the-small-web/) apps.

        [Free as in freedom](https://codeberg.org/kitten/app), small as in [Small Technology](https://small-tech.org/about/#small-technology).

        <div class='prereleaseMessage'>
        🍼 Kitten is still a baby. Expect breaking changes until API version 1.
        </div>

        ## Get started
      </markdown>

      <details>
        <summary>System requirements</summary>
        <div>
          <markdown>
            - **Linux** and **macOS**.
            - **Bash version 5.x+**.
            - **Common developer tools and system utilities** ([code]git[code], [code]tar[code], [code]tee[code], and [code]xz[code]).
          </markdown>
        </div>
      </details>

      <${InstallationInstructions} platform=${platform} version=${version} />

      <markdown>
        ### Play

        <video controls="" preload="none" src="https://player.vimeo.com/progressive_redirect/playback/920601063/rendition/1080p/file.mp4?loc=external&log_user=0&signature=9559ca051a11bc361bafbb9b2542248f358cfa592367c0efa3626d0a164cde81" poster="/videos/streaming-html/poster.jpg" width="800">
            <track label="English captions" king="subtitles" srclang="en" src="/videos/streaming-html/transcript.vtt" default="">
            <img src="/videos/streaming-html/poster.jpg" alt="" width="640" height="360">
            <p><a href="https://player.vimeo.com/progressive_redirect/download/920601063/rendition/1080p/kitten:_streaming_html_%E2%80%93_counter_example%20%281080p%29.mp4?loc=external&signature=efa1c6415c362590b2a2c904aad8dafe6ff048fef2db80c6d12140fd6fc4a999">Download the video</a> and watch it with your favourite video player.</p>
        </video>

        Kitten, uniquely, enables you to build Small Web apps (peer-to-peer web apps). But it also aims to make creating any type of web app as easy as possible. Before embarking on the [Tutorials](/tutorials) from the beginning, let’s jump right in at the deep end and create a simple app using Kitten’s [Streaming HTML](/tutorials/streaming-html) workflow.

        #### The ubiquitous counter example

        1. Create a directory for the example and enter it:

           [code shell]
           mkdir counter
           cd counter
           [code]

        2. Create a file called _index.page.js_ and add the following content to it:

            [code js]
            if (kitten.db.counter === undefined) kitten.db.counter = { count: 0 }

            export default () => kitten.html\`
              <page css>
              <h1>Counter</h1>
              <\${Count} />
              <button name='update' connect data='{value: -1}' aria-label='decrement'>-</button>
              <button name='update' connect data='{value: 1}' aria-label='increment'>+</button>
            \`

            const Count = () => kitten.html\`
              <div
                id='counter'
                aria-live='assertive'
                morph
                style='font-size: 3em; margin: 0.25em 0;'
              >
                \${kitten.db.counter.count}
              </div>
            \`

            export function onUpdate (data) {
              kitten.db.counter.count += data.value
              this.send(kitten.html\`<\${Count} />\`)
            }
            [code]

        3. Run Kitten using the following syntax:

           [code shell]
           kitten
           [code]

        > 💡 In the video, you see a slighty older and more verbose way of adding an event handler. Going forward, you should prefer the method shown in the code example here.

        Once Kitten is running, hit *[https://localhost](https://localhost)*, and you should see a counter at zero and two buttons.

        Press the increment and decrement buttons and you should see it count update accordingly.

        Press <kbd>Ctrl</kbd> <kbd>C</kbd> in the terminal to stop the server and then run [code]kitten[code] again.

        Refresh the page to see that the count has persisted.

        In two dozen lines of liberally-spaced code, you have built a very simple web application that:

        - Is fully accessible (turn on your screen reader and have a play).

        - Persists data to a database.

        - Triggers events on the server in response to button presses and sends custom data from the client to the server.

        - Sends an updated [code]Count[code] component back to the client which automatically gets morphed into place, maintaining state.

        - Uses a basic semantic CSS library to style itself.

        - Uses WebSockets, [htmx](https://htmx.org/), and [Water](https://watercss.kognise.dev/) behind the scenes to achieve its magic.

        Now you have some idea of what to expect, let’s slow down and [start from the beginning with the tutorials](/tutorials). As you’ll see, the [Streaming HTML](/tutorials/streaming-html) workflow shown above is just one streamlined way of working with Kitten.

        Kitten is designed in a progressively enhanced manner so that it will serve static HTML sites and traditional web sites written in HTML, CSS, and JavaScript, as well as peer-to-peer Small Web sites and ones that make sure of the special enhancements that Kitten provides to make web development simple and fun again.

        ## Like this? Fund us!

        [Small Technology Foundation](https://small-tech.org) is a tiny, independent not-for-profit.

        We exist in part thanks to patronage by people like you. If you share [our vision](https://small-tech.org/about/#small-technology) and want to support our work, please [become a patron or donate to us](https://small-tech.org/fund-us) today and help us continue to exist.

      </markdown>
    </>
  `
}
