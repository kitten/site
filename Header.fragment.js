import kitten from '@small-web/kitten'
import Navigation from './Navigation.fragment.js'
import Styles from './Header.fragment.css'

export default function ({pageId, skipToContentId = 'name'}) {
  return kitten.html`
    <header>
      <img
        id='logo'
        src='/images/kitten-head.svg'
        alt='Kitten logo: a minimalist illustation of an adorable kitten’s head with pink nose and ears.'
      >
      <h1>Kitten</h1>
      <a href='#${skipToContentId}' class='skip-link'>Skip to content.</a>
      <${Navigation} pageId=${pageId} />
      <${Styles} />
    </header>
  `
}
