---
layout: '../Main.layout.js'
pageId: faq
---
## Frequently-Asked Questions (FAQs)

### What about serverless?

Dude, this is literally a server.

_If you want “serverless” (funny how folks who own servers want you to go serverless, isn’t it? It’s almost like a small group of people get to own stuff and you have to rent from them on their terms… hmm 🤔️) then use some Big Tech framework like [SvelteKit](https://kit.svelte.dev). They’ll bend over backwards to cater to all your Big Web needs._

### Can you add &lt;insert Big Tech feature here&gt;?

No.

### Will this scale?

_\*le sigh!\*_

(Yes, it will scale for the purposes it was designed for. It will not scale for the purposes of farming the population for their data and destroying our human rights and democracy in the process. That’s a feature, not a bug.)

### Is there anything stopping me from using this to build sites or apps that violate people’s privacy and farm them for their data? (You know, the business model of Silicon Valley… you know the whole surveillance capitalism and people farming thing?)

No, there is nothing in the license to stop you from doing so.

But I will haunt you in your nightmares if you do.

(Just sayin’)

Also, it’s not nice. Don’t.

### Is this really a Frequently Asked Questions section or a political statement?

Can’t it be both?

(It’s a political statement.)

## Questions?

Contact <a href="https://mastodon.ar.al/@aral" rel="me">Aral</a> on the fediverse.
