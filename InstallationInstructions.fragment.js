import { html } from '@small-web/kitten'
import Platform from './Platform.script.js'

const CodeWithCopyButton = ({code, platform}) => html`
  <pre class='codeWithCopyButton'>
    <code class='hljs language-shell'>${[code]}</code>
    <button
      name='copy'
      onclick="navigator.clipboard.writeText(\`${code}\`)"
      connect
      data='{platform: "${platform}"}'
    >Copy</button>
    <style>
      .codeWithCopyButton {
        display: flex;
        flex-direction: row;
        column-gap: 1em;

        button {
          margin: 0;
        }
      }
    </style>
  </pre>
` 

export default function InstallationInstructions ({ platform, version }) {
  return html`
    <section
      id='installation-instructions'
      aria-live='assertive'
      morph
    >
      <markdown>
        ### Install
      </markdown>

      <if ${version == undefined}>
        <then>
          <div class='error'>
            <markdown>
              __Sorry, [the Kitten download site](https://kittens.small-web.org) appears to be down at the moment.__

              Please wait a moment and [try again](/).

              If the issue doesn’t resolve itself shortly, please [send us an email](mailto://hello@small-tech.org) and let us know.
            </markdown>
          </div>
        </then>
        <else>
          <p><a href='https://kittens.small-web.org'>Latest version</a> released ${version.date} ${Intl.DateTimeFormat().resolvedOptions().timeZone} (<a href='${version.link}' target='_blank'>view source</a>).</p>

          <ul id='platform-tabs'>
            <li>
              <button
                name='platform'
                connect
                data='{value: "Linux"}'
                aria-current=${platform.is(Platform.LINUX) || platform.is(Platform.OTHER) ? 'true' : 'false'}
              >Linux</button>
            </li>
            <li>
              <button
                name='platform'
                connect
                data='{value: "Macintosh"}'
                aria-current=${platform.is(Platform.MAC) ? 'true' : 'false'}
              >macOS</button>
            </li>
            <li>
              <button
                name='platform'
                connect
                data='{value: "Windows"}'
                aria-current=${platform.is(Platform.WINDOWS) ? 'true' : 'false'}
              >Windows</button>
            </li>
          </ul>

          <if ${platform.is(Platform.LINUX) || platform.is(Platform.OTHER)}>
            <then>
              <div class='installationInstructions'>
                <markdown>
                  Run the following command in a terminal window (or, use <a href='' name='app' connect data='{value: "curl"}'>curl</a>):
                </markdown>
                <${CodeWithCopyButton}
                  code='wget -qO- https://kittens.small-web.org/install | bash'
                  platform=${platform.key}
                />
              </div>
            </then>
          </if>

          <if ${platform.is(Platform.MAC)}>
            <then>
              <div class='installationInstructions'>
                <markdown>
                  > 💡 **macOS** comes with an ancient version of Bash. To upgrade it, run [code]brew install bash[code] using [Homebrew](https://brew.sh) before you install Kitten.

                  Run the following command in a terminal window:
                </markdown>
                <${CodeWithCopyButton}
                  code='curl -sL https://kittens.small-web.org/install | bash'
                  platform=${platform.key}
                />
              </div>
            </then>
          </if>

          <if ${platform.is(Platform.WINDOWS)}>
            <then>
              <div class='installationInstructions'>
                <markdown>
                  > 🙀 __Windows is an ad-infested and surveillance-ridden dumpster fire of an operating system and you are putting both yourself and others at risk by using it.__

                  Kitten is not supported on Windows although [you may be able to get it running under WSL 2](/reference/#install-kitten-on-windows-under-wsl-2).

                  _Please do not open issues for Windows-specific issues as they will not be fixed._
                </markdown>
              </div>
            </then>
          </if>
        </else>
      </if>
      <style>
        .error::before {
          content:'😿';
          font-size: 6rem;
          float: left;
          margin-right: 1rem;
        }
      </style>
    </section>
  `
}
