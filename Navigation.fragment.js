import { html } from '@small-web/kitten'
import Styles from './Navigation.fragment.css'

const pages = [
  {id: 'home', link: '/', title: 'Home'},
  {id: 'tutorials', link: '/tutorials', title: 'Tutorials'},
  {id: 'reference', link: '/reference', title: 'Reference'},
  {id: 'credits', link: '/credits', title: 'Credits'},
  {id: 'faq', link: '/faq', title: 'FAQ'}
]

export default ({pageId}) => {
  return html`
    <nav aria-label='Main'>
      <ul>
        ${pages.map(page => html`
          <if ${pageId === page.id}>
            <then>
              <page title='Kitten: ${page.title}'>
              <li class='selected'>${page.title}</li>
            </then>
            <else>
              <li><a href='${page.link}'>${page.title}</a></li>
            </else>
          </if>
        `)}
        <li><a href='https://codeberg.org/kitten/app/' target='_blank'>Source</a></li>
        <li><a href='https://small-tech.org/fund-us/' target='_blank'>Fund Us</a></li>
      </ul>
    </nav>
    <${Styles} />
  `
}
