import Main from '../Main.layout.js'
import Styles from './Reference.fragment.css'

export default ({ SLOT, ...props }) => {
  return kitten.html`
    <${Main} skipToContentId='about' ...${props}>
      ${SLOT}
      <${Styles} />
    </>
  `
}
