---
title: Reference
layout: './Reference.layout.js'
pageId: reference
---
## Reference

  > 🚧 The reference section is under development and should in no way, shape, or form, be considered to be exhaustive at the moment.


  > 📖 This reference guide lists the features of Kitten. For a comprehensive “owner’s manual”-style technical reference of how Kitten is designed and works internally, please see the [Technical Manual](/reference/technical-manual).

[toc]

## About

Kitten is a little kit (“kitten”, get it?) optimised for people (not corporations) who make the new everyday things for themselves and other people – not because they want to become billionaires but because they want to contribute to the common good by helping nurture the [Small Web](https://ar.al/2020/08/07/what-is-the-small-web/) while adhering to the [Small Technology Principles](https://small-tech.org/about/#small-technology).

Kitten features intuitive file system-based routing, HTML and accessibility validation, a built-in object database, WebSockets, zero-code authentication, public-key cryptography, Markdown support, syntax highlighting via [highlight.js](https://highlightjs.org/), semantic styles via [Water CSS](https://watercss.kognise.dev/), and more.

## System requirements

- **Linux** and **macOS**.
- **Bash version 5.x+**.
- **Common developer tools and system utilities** (`git`, `tar`, `tee`, and `xz`).

> 💡 **macOS** comes with an ancient version of Bash. To upgrade it to a modern one, run `brew install bash` using [Homebrew](https://brew.sh).
> 
> 💡For [production servers](#deploying-a-kitten-application), only Linux with systemd is supported.

## Install

To install Kitten, _either_:

- 😇 Clone the Kitten repository and run the `./install` script:

    ```shell
    git clone https://codeberg.org/kitten/app kitten
    cd kitten
    ./install
    ```

- 😈 Or, copy and paste one of the following commands into your terminal to pipe the installation script into your shell and run it:

    #### Using wget:

    ```shell
    wget -qO- https://kittens.small-web.org/install | bash
    ```

    #### Using curl:

    ```shell
    curl -sL https://kittens.small-web.org/install | bash
    ```

    > 🤓 ***You can download and view the install script source to verify it’s safe to pipe to your shell: https://kittens.small-web.org/install. And also compare it to: https://codeberg.org/kitten/app/raw/branch/main/install*** 

    #### Post-installation steps (macOS)

    On macOS, you have a couple of manual post-installation steps to perform.
    
    (Detailed instructions on how to carry out the below steps are presented in the installer at installation time.)

    1. __You must update your system’s path with the location of the kitten binary__ as _~/.local/bin_ is not on the path by default on macOS.
    
        (You will also have to do this on Linux distributions that don’t automatically add this folder to your system path. The installer will prompt you to do this if it detects that this is the case.)

    2. __If you want to test peer-to-peer apps locally, you must edit your _~/etc/hosts_ file__ to add Kitten’s localhost aliases (_place1.localhost_, _place2.localhost_, …, _place4.localhost_).

        > 💡 To learn more about building peer-to-peer Small Web apps, see the [End-to-end encrypted peer-to-peer Small Web apps tutorial](/tutorials/end-to-end-encrypted-peer-to-peer-small-web-apps/).

    > 💡If you want to install not the latest version of Kitten but the latest version for a given API version, modify your download command to tack the api version to the URL and to pass it as an argument to the bash script. For example, if you wanted to install the latest Kitten package for API version 1 using CURL, your command would look like this:
    >
    > ```shell
    > curl -sL https://kittens.small-web.org/install/1/ | bash -s -- 1
    > ```

> 💡If you’re running an “immutable” Linux distribution like Fedora Silverblue, please install Kitten from your host account (instead of from within a container) at least once so Kitten can set [unprivileged ports](https://ar.al/2022/08/30/dear-linux-privileged-ports-must-die/) to start from 80 (so it can run without elevated privileges).

## Update

You can update Kitten in a variety of ways:

  - If you use the command-line during development, run the `kitten update` command.

  - If you want to manually update Kitten in a deployed app, you can use the web interface on the Kitten Settings page (_/🐱/settings/kitten/_).

  - If you have a deployed app that uses [Kitten Versioning](#versioning), both Kitten and the installed app will be automatically updated. These updates will ensure that Kitten is not updated if the installed app itself has not been updated to support the latest Kitten API version to ensure that deployed apps do not break due to breaking changes in major API version of Kitten.

    _Note that this API version guarantee will only come into effect after Kitten reaches API version 1. Kitten is currently in pre-release at API version 0._

## HTML

Kitten has a semi-forgiving HTML parser.

  - ### XML-style closing tags are not required for regular HTML tags.
  
    For example, you can use `<img>` instead of `<img />`.
  
  - ### For custom Kitten components, you must close your tags.
  
    For example, `<${MyComponent} />` is valid, `<${MyComponent}>` is _not_.

  - ### The parser will make a best-effort to close unclosed tags.
  
    So, for example, it will replace `<p>Hello` with `<p>Hello</p>` but it’s not foolproof.
    
    You will get an _unexpected close tag_ error if you put [flow content](https://html.spec.whatwg.org/#flow-content-2) where [phrasing content](https://html.spec.whatwg.org/#phrasing-content) is expected (e.g., [a `<div>` inside a `<p>` tag](https://caninclude.glitch.me/caninclude?child=div&parent=p), etc.)

  - ### Comments in HTML are stripped from the final output.

    There’s currently no way to include comments in the markup generated by Kitten.

### Request and response helpers

Kitten has a number of request and response helpers defined to make your life easier.

> 💡 You use two of them, `response.forbidden()` and `response.get()` in the [authentication tutorial](/tutorials/authentication).

#### Request

- **`is (array|string)`**: returns true/false based on whether the `content-type` of the request matches the string or array of strings presented.
This is used internally for Express Busboy compatibility but you might find it useful in your apps too if you’re doing low-level request handling.

#### Response

- **`withCode(code, [body])`: Shorthand for setting `.statusCode` on `response` and calling `response.end()` with an optional body. Useful when returning custom HTTP Status Codes like the ones used in the Small Web Protocol.

- **`ok()`**: Returns 200 OK.

- **`created()`**: Returns 201 Created.

- **`json (data)`**: Returns a JSON serialised response of the passed data.

- **`jsonFile (data)`**: Triggers a download of the passed data in JSON format. 

- **`file (data, fileName?, mimeType?)`**: Sends downloadable file (default name is `download` and default MIME type is `application/octet-stream`).

- **`get (location)`**, `seeOther (location)`: 303 See Other redirect (always uses GET).

- **`redirect (location)`**, `temporaryRedirect (location)`: 307 Temporary Redirect (does not change the request method).

- **`permanentRedirect (location)`**: 308 Permanent Redirect (does not change the request method)

- **`badRequest (body?)`**: 400 Bad Request.

- **`unathenticated (body?)`**, `unauthorised (body?)`, `unauthorized (body?)`: 401 Unauthorized (unauthenticated).

- **`forbidden (body?)`**: 403 Forbidden (request is authenticated but lacks sufficient rights – i.e., authorisation – to access the resource).

- **`notFound (body?)`**: 404 Not Found.

- **`error (body?)`**, `internalServerError (body?)`: 500 Internal Server Error.

## Streaming HTML

Kitten gives you a simple-to-use event-based HTML over WebSocket implementation called Streaming HTML (because you’re streaming HTML updates to the client) that you can use to build web apps.

Please see the [Streaming HTML tutorial](/tutorials/streaming-html/) for details.

## Kitten components

### Simple components

A Kitten component is any function that returns `kitten.html`.

So this can be as simple as:

```js
const SayHello = () => kitten.html`
  <h1>Hello</h1>
`
```

Of course, static components are not very useful, so component functions usually take a parameter object with properties (or “props”, for short):

```js
const SayAnything = ({ thingToSay = 'Hello' } = {}) => kitten.html`
  <h1>${thingToSay}</h1>
`

console.log(kitten.html`<${SayAnything} thingToSay='Goodbye' />`
```

The above example, for example, will output:

```html
<h1>Goodbye</h1>
```

### `kitten.Component` components

While simple components are great, if you use Kitten’s [Streaming HTML workflow](/tutorials/streaming-html/), you may find yourself having to do a bit of manual work if you want to use simple components and wire them up to listen for events from connected pages. This will become especially true if you carry out precise updates, morphing small bits of your interface in response to events and do so in a maintainable manner by breaking up your interface into small, well-encapsulated components.

To make this easier for you, Kitten provides the `Component` class that you can reference in your apps from the global `kitten` namespace.

> 💡 Remember that you can get language intelligence and strong typing for your apps by importing the type safe version of the `kitten` global from the `@small-web/kitten` module.

For a real-world example of the use of the `kitten.Component` class, please see the [Profile section code of Place](https://codeberg.org/place/app/src/branch/main/settings%F0%9F%94%92/profile/index.page.js).

## Markdown support

Kitten’s Markdown support comes from [markdown-it](https://github.com/markdown-it/markdown-it#markdown-it-).

Kitten supports a very rich set of Markdown features including:

- [automatic anchors for headings](https://github.com/valeriangalliat/markdown-it-anchor#markdown-it-anchor-)
- [syntax highlighting](https://github.com/valeriangalliat/markdown-it-highlightjs#readme)
- [figure (and figure caption)](https://github.com/Antonio-Laguna/markdown-it-image-figures#markdown-it-image-figures) support
- [footnotes](https://github.com/markdown-it/markdown-it-footnote#readme)
- typographical niceties (like converting typewriter quotes "" into curly quotes “”, 
- [insert](https://github.com/markdown-it/markdown-it-ins#readme)
- [mark](https://github.com/markdown-it/markdown-it-mark#readme)
- [subscript](https://github.com/markdown-it/markdown-it-sub#markdown-it-sub)
- [superscript](https://github.com/markdown-it/markdown-it-sup#markdown-it-sup), etc.)
- and even [table of contents creation](https://github.com/nagaozen/markdown-it-toc-done-right#readme) for Markdown sections.

You can use Markdown in your Kitten apps in a variety of ways:

### Markdown pages (_.page.md_ files).

You can create whole pages using Markdown by placing your Markdown in _page.md_ files. These pages are transpiled into JavaScript before being transpiled into HTML just like regular JavaScript pages (_page.js_) files.

Your Markdown pages can have front matter in [YAML](https://en.wikipedia.org/wiki/YAML) format.

> 💡Kitten’s YAML parser is based on the Node [yaml](https://www.npmjs.com/package/yaml) module and you can use it directly in your own Kitten apps by referencing from `kitten.yaml`.

#### Front matter

Kitten’s YAML front matter supports some special properties. Specifically:

##### `layout` _(string)_

Specifies the layout template to use for the Markdown page.

The value for the layout property is a string that contains the relative path to the layout template (_.layout.js_ file) you want to import and use.

Any [custom properties in front matter](#custom-properties-in-front-matter) are passed to the layout template as properties.

For example, if your layout template is called _Main.layout.js_ and it is located in the _layouts/_ folder of your project and you want to include it in a page called _index.page.md_ at the root of your project, your front matter might look like this:

```markdown
---
layout: './layouts/Main.layout.js'
title: 'Feed me!'
author: 'Oskar Kalbag'
---

Woof.
```

And your layout template might look like this:

```js
export default function MainLayout ({ SLOT, title, author } = {}) {
  return kitten.html`
    <header>
      <h1>Oskar’s blog</h1>
      <p>I am a dog.</p>
    </header>
    
    <main>
      <h2>${title} by ${author}</h2>
      ${SLOT}
    </main>

    <footer>
      <p>PS. Have you fed me?</p>
    </footer>
  `
}
````

> 💡 In addition to custom properties in front matter (like `title` and `author` in the example above) being passed to the layout template, three built in properties are also passed:
>
> - `page`: reference to the current page, if this is a live/connected page ([KittenPage](https://codeberg.org/kitten/globals/src/branch/main/types.d.ts#L263) instance.)
>
> - `request`: reference to the [KittenRequest](https://codeberg.org/kitten/globals/src/branch/main/types.d.ts#L374) object. (A Node.js [http.IncomingMessage](https://nodejs.org/docs/latest/api/http.html#class-httpincomingmessage) object as [as extended by Polka](https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/polka/index.d.ts#L24) and then again by Kitten. The important Kitten addition is the `session` property that refers to the current session.)
>
> - `response`: reference to the [KittenResponse]() object. (A Node.js [http.ServerResponse](https://nodejs.org/docs/latest/api/http.html#class-httpserverresponse) object as extended by Kitten. Kitten mixes in a number of useful [response helpers](https://kitten.small-web.org/reference/#response).
>
> When using JavaScript-based live pages, you can get references to the page from `this.page` and to the request and response objects via `this.page.request` and `this.page.response` from any connected component. However, since Markdown pages are not live pages, Kitten passes the references for you and you have no way of passing them to your layout component yourself in Markdown.

##### `import` _(list)_

You can import components and fragments and use them in your Markdown pages just as you would in regular Kitten pages.

> 🪤 The current limitation is that they cannot contain child content of their own – so no slots for the time being.

The `import` list contains JavaScript import statements in the following form:

```markdown
---
imports:
  - import ComponentName from '../path/to/ComponentName.component.js
  - import FragmentName from '../path/to/FragmentName.fragment.js'
---
```
Once imported as shown above, you can use then using them in your Markdown page as you would normally:

```markdown
# Components and fragments

This is a component:

<${ComponentName} prop=someValue anotherProp=someOtherValue />

And this is a fragment:

<${FragmentName} prop=someValue anotherProp=someOtherValue />
```

#### Custom properties in front matter

Any properties outside of the special ones listed above are passed to your page’s layout template (if it has one) as props.

### Markdown fragments

You can place static pieces of Markdown into Markdown fragment files (_fragment.md_).

Unlike Markdown pages, Markdown fragments do not support front matter or template substitution. They are purely for static content. 

### Markdown in Kitten HTML tagged template strings

Finally, you can embed Markdown directly in your Kitten HTML tagged template string by surrounding your Markdown content between `<markdown>…</markdown>` tags.

e.g., 

```js
export default () => kitten.html`
<h1>This is HTML</h1>
<markdown>
  - _This_
  - is
  - __Markdown!__
</markdown>
<footer>
  <p>This is more HTML</p>
</footer>
`
```
> 💡 Unlike HTML, Markdown is whitespace-sensitive so make sure you indent your code properly or it will get parsed incorrectly.

> 💡 ### Syntax highlighting
>
> Syntax highlighting in Markdown is provided via [Highlight.js](https://highlightjs.org).
>
> The default [themes](https://highlightjs.readthedocs.io/en/latest/theme-guide.html) for light and dark mode are the [Measured Light and Measured Dark](https://measured.co/blog/accessible-syntax-highlighting-colour-schemes-for-developers) accessible syntax highlighting colour scheme implementations [for Highlight.js](https://github.com/measuredco/base16-measured-scheme/tree/main/built/highlightjs/themes).
>
> You can override this setting by setting the `syntax-highlighter-theme-light` and `syntax-highlighter-theme-dark` attributes of your `<page>` tag to the absolute or relative path to the respective theme CSS file.
>
> [View the full list of default Highlight.js themes.](https://github.com/highlightjs/highlight.js/tree/main/src/styles)

You can see some of the Markdown features mentioned here demonstrated in the [examples/components](https://codeberg.org/kitten/app/src/branch/main/examples/components) and [examples/markdown](https://codeberg.org/kitten/app/src/branch/main/examples/markdown) folders of the [Kitten examples](https://codeberg.org/kitten/app/src/branch/main/examples), as well as in the [source for the Kitten web site itself](https://codeberg.org/kitten/site).

## Deploying a Kitten application

To deploy your app as a service, you use the `deploy` command:

```shell
kitten deploy <httpsGitCloneURL>
```

For example, the following will create and run a production server of the [Persisted Kitten Chat](#persisted-kitten-chat) example, [as hosted on my personal Codeberg account](https://codeberg.org/aral/persisted-kitten-chat) at my hostname.

```
kitten deploy https://codeberg.org/aral/persisted-kitten-chat
```

## Updating a deployed Kitten application

There are a number of ways you may wish to update your deployed Kitten application, depending on who the audience for your app or site is.

  - If you’re making a Small Web app that you envision being used by everyday people who use technology as an everyday thing, then you will want to implement [Kitten versioning](#versioning) in your Git source code repositories.
    
    > 😻 If you use Kitten versioning in your apps, Kitten periodically and automatically checks for updates to your deployed app and makes sure your deployment is up-to-date. 

  - If you are building something for yourself and you’re happy to manually update the site, you can do so from the App Settings page of your Kitten site (_/🐱/settings/app/_).  

  - If you’re building something for yourself and you want your Kitten site to update every time you push to the main branch of your Git repository, you can set up a webhook.

### 🪝Webhook

You can set up a webhook on your remote Git source code repository that updates the site whenever you perform a certain git action (e.g., push to main).

You can find your webhook URL and webhook secret on the App Settings page of your Kitten site (_/🐱/settings/app/_).

Your Kitten site expects the webhook secret in either the `Authorization` header or in the body of a POST request in a field called `secret` (the body should be `application/x-www-form-urlencoded`).

e.g., Instructions for [Codeberg](https://codeberg.org):

1. Enter your _Webhook URL_ into the _Target URL_ field.
2. Set the _HTTP method_ to `POST`.
3. Set the _POST content type_ to `application/x-www-form-urlencoded`.
4. Leave the _Secret_ field empty.
5. Set _Trigger on_ to `Push events`
6. Set Branch filter to `main` (or set to a separate branch you want to deploy from)
7. Enter your _Webhook secret_ into the _Authorization header_ field.
8. Make sure the _Active_ checkbox is checked.
9. Press the _Add webhook_ button.

Once the webhook has been successfully added, your site should update whenever you `git push` to `main`.

This is a feature that will be useful to developers who are deploying their own sites and apps for their own use as opposed to developers making apps for use by everyday people who use technology as an everyday thing. For the latter use case, please see [Versioning](#versioning), below.

### Versioning


> 💡 If you want to quickly test out a Kitten app that doesn’t have versioning information, use the `kitten run` command instead.

You version your Small Web apps using git tags:

Tag your Small Web apps in the following format to version them:

``` 
<Kitten API Version>.<App Version>
```

Where:

- `Kitten API Version` is the version of the Kitten API that your app is compatible with.
- `App Version` is the version of your app, a monotonically increasing integer that starts at `1`.

To tag your app, simply use the `git tag` command as usual, e.g.,

```
git tag -s 1.3
```

The above tag will label your current commit as being compatible with Kitten API Version `1` and having an app version of `3`.

If a deployed version of your app is running version `1.2`, it will __not__ be updated when you push commits to your main branch, etc. It will also __not__ update the installed version of Kitten if, say, a new Kitten package is released that has an API Version of 2. Kitten will only be upgraded to the latest version when you release a version of your app that’s tagged with support for Kitten API Version 2. e.g.,

```  
git tag -s 2.1
```

Automatic updates of Small Web places and of Kitten itself follows one of the core tenets of the Small Web which is that Small Web places should not require technical knowledge to maintain.

As such, updates should never a break a Small Web place and require technical intervention to fix.

This means that your Small Web apps must never break backwards compatibility. So, for example, if you have schema changes in your database, you must write migration code to give existing data the shape that the latest version of your code requires, etc.

>  💡 Kitten productions servers periodically and automatically check for updates to both Kitten itself and to the Kitten app that is being served. Whenever there is a breaking change to the Kitten API (e.g., an existing method is removed or how it’s used is changed in such a manner that it could break existing code), the change is documented in the change log and the Kitten API version, which is a monotonically increasing integer, is incremented by one.
>
> To see how it works, imagine that you deploy a new server running a Kitten version that has its API version set to 1. You specify this API version in your git tag as shown above. Whenever Kitten checks for Kitten updates, it ensures that the API version is still 1 before updating Kitten. Say that after three Kitten updates, Kitten notices that the latest version of Kitten has an API version of 2 but that your app (which Kitten is also periodically checking for updates for from its git repository) is still at API version 1. At that point, Kitten does not install the latest version of Kitten but rather waits for you to test your app with Kitten API version 2 and specify that it is supported by creating a new git tag accordingly.
>
> If there are any other Kitten packages released that support Kitten API version 1 (e.g., bug/security fixes that are backported from the latest versions of Kitten), Kitten will continue to update itself to those versions.
>
> Once Kitten sees that you’ve updated your app to Kitten API version 2, it will go ahead and update itself to the latest package supported by that version.
>
> So, all this to say that Kitten tries its hardest to ensure that its auto-updates system does not break your servers.
>
> 🪤 If you are using a database in your app, please remember that you are responsible for ensuring that schema changes do not break your app between versions. In other words, you are responsible for implementing migrations. To make this easier on yourself, you might want to ensure that the collections and individual data items you persist are custom classes that abstract away the actual objects that are persisted. That way, you can implement granular migrations in your model classes. Otherwise, you can, of course, also implement global, one-shot migrations in the traditional manner.
>
> 💡️ Production servers require systemd.
>
> 💡️ Kitten will not automatically create an alias for www for you, pass `--aliases=www.<hostname>` (or the shorthand, `--aliases=www`) if you want that. You can, of course, also list any other subdomains other that _www_ that you want Kitten to serve your site/app on in addition to the main domain.
>
> 😻 If your app uses node modules, Kitten will intelligently call `npm install` on your project, as well as on any app modules your project might have.

## 🌲 Evergreen Web (404 → 307)

Kitten has built-in support for the Evergreen Web (404 to 307) technique.

__To implement 404 to 307 redirection for your Small Web place in Kitten, simply set domain you want to forward to in your Small Web Settings page which you can reach in your browser at `/🐱/settings/`.__

Let’s say you are deploying your new Small Web place at your own domain (e.g., https://ar.al) but you already have a personal web site or a blog there. You will already have content that other people might have linked to and/or rely on. It would be a shame for all those links to break when you deploy your new site.

The Evergreen Web (404 to 307) technique is very simple:

On your new site, issue a 307 redirect for any paths that result in a 404 (not found) error to the previous version of your site. That way, if the path existed in the previous version of your site, it will be found there and, if it did not, it will result in a 404 error there.

> 💡 Of course, you’re not limited to one level of indirection. If the previous version of your site replaced an even earlier version, there’s no reason why you cannot add a 404 to 307 rule there also and have a chain of redirects going all the way back to the earliest version of your site.
>
> One of the great advantages of this technique is that you can just leave older versions of your site running (perhaps at different subdomains) and they can all continue to exist using whatever technologies they were using at the time. (You don’t have to take static exports, etc., of the data.)

By using 404 to 307, you will contribute to an evergreen web by not breaking URLs.

To see it in action, run the [evergreen-web example](https://codeberg.org/kitten/app/src/branch/main/examples/evergreen-web).

For more information, see [4042307.org](https://4042307.org).

## Valid file types

Kitten doesn’t force you to put different types of routes into predefined folders. Instead, it uses file extensions to know how to handle different routes and other code and assets.

Here is a list of the main file types Kitten handles and how it handles them:

| Extension          | Type                           | Behaviour                     |
| ------------------ | ------------------------------ | ----------------------------- |
| .page.js           | Kitten page (JavaScript)       | JavaScript route that is compiled into HTML and served in response to a HTTP GET request for the specified path. |
| .page.md           | Kitten page (Markdown)         | Markdown route with optional front matter that supports JavaScript imports and the use of Kitten components and fragments in Markdown that is compiled into HTML and served in response to a HTTP GET request for the specified path. |
| .post.js, .get.js, .put.js, .head.js, .patch.js, .options.js, .connect.js, .delete.js, .trace.js | HTTP route | Served in response to an HTTP request for the specified method and path. |
| .socket.js         | WebSocket route                | Served in response to a WebSocket request for the specified path. Only file type to keep part of its file extension in its route pattern (e.g., the file _/my.socket.js_ exists at the URL fragment _/my.socket_.)|
| .component.js      | A component file, returns HTML | Ignored by router.            |
| .layout.js         | Layout component, returns HTML | Ignored by router.            |
| .fragment.js       | A fragment file, returns HTML  | Ignored by router.            |
| .fragment.html     | An HTML fragment file          | Ignored by router.            |
| .fragment.css      | A CSS fragment file            | Ignored by router.            |
| .fragment.md       | A Markdown fragment file       | Ignored by router.            |
| .script.js         | Server-side script file        | Ignored by router. Useful for including server-side JavaScript modules. File is not accessible from the client. |
| .styles.js         | Server-side styles file        | Ignored by router. Useful for including server-side CSS (in JS). File is not accessible from the client.        |
| Other (.html, .css, .js, .jpg, .gif, etc.)          | Static files | Any other files in your project apart from the ones listed above are served as static files.                    |

## HTTP routes

HTTP data routes are served in response to an HTTP request for the specified method and path.

All HTTP request methods are supported.

You create an HTTP route by create a JavaScript file named with the HTTP request method you want to respond to.

For example, to respond to GET requests at _/books_, you would create a file named _books.get.js_ in the root of your source folder.

The content of HTTP routes is an ESM module that exports a standard Node route request handler that takes [http.IncomingMessage](https://nodejs.org/api/http.html#http_class_http_incomingmessage) and [http.ServerResponse](https://nodejs.org/api/http.html#http_class_http_serverresponse) arguments.

For example, your _books.get.js_ route might look like this:

```js
export default ({ request, response }) => {
  const books = kitten.db.books.get()
  response.end(books)
}
```

## URL normalisation

Kitten automatically normalises URLs that do not have a trailing slash when they should to add one.

It does so using a permanent 308 redirect that preserves the method of the request. 

This means, for example, that a request to _https://my.site/hello_ will be forwarded to _https://my.site/hello/_.

This is both to help implement canonical paths for your sites/apps and to avoid [the unexpected situation of a relative include failing from your page if a slash is not provided in the path](https://stackoverflow.com/questions/31111257/relative-links-on-pages-without-trailing-slash). (e.g., if you have an _index.js_ file and an _index.html_ file in a folder called _hello_ and you include the JavaScript file using `<script src='./index.js'></script>`, that will succeed if the page is hit as _/hello/_ but fail if it is hit as _/hello_.)

## Sessions and authentication

Kitten automatically manages sessions and authentication for you.

You can add a lock emoji (🔒) to the end of any route to make it an authenticated route.

Authenticated routes are only accessible by the owner of the site after they sign in using their secret.

> 💡 If an authenticated route is a folder, any child routes also require authentication.

You can also manually check if the owner of the site is signed in by checking if `request.session.authenticated` is `true` or not. 

Finally, you can manually direct people to sign in and out by redirecting them to the following routes:

  - `/💕/sign-in`
  - `/💕/sign-out`

> 💡 Remember that the Small Web – and therefore, Kitten – does not have the concept of users. There is only ever one _owner_ of the site, who is a person. And only this owner can sign into the site. The site might be viewed by countless people over time but they cannot authenticate with it or create accounts on it.
>
> (If you want to build this functionality yourself, for your app, you of course can but it is neither encouraged nor supported. Please see the [FAQ](/faq) for more details.)
>
> The idea is that everyone has their own Small Web site and, when they want to communicate with each other, they do so in a peer-to-peer fashion.
>
> This is the core difference between the Big Web – which is made of up of centralised silos often owned by trillion-dollar and multi-billion-dollar corporations that make their money by farming you for your data – and the Small Web, which is a peer-to-peer web of people who use technology as an everyday tool to communicate with each other and enrich their lives.
>
> To understand this better, please see the [End-to-end encrypted peer-to-peer Small Web apps tutorial](/tutorials/end-to-end-encrypted-peer-to-peer-small-web-apps/).

For more details, please see the following two tutorials:

  - [Sessions tutorial](/tutorials/sessions/)
  - [Authentication tutorial](/tutorials/authentication/)

## Data scopes

In Kitten, you have a number of different scopes, with varying lifetimes, where you can store data.

These are, from most limited and ephemeral to widest and most permanent:

| Scope | Lifetime | Availability | Usage | Example |
| ----- | -------- | ------------ | ----- | ------- |
| Page storage | Life of a single page loaded in a browser window/tab | Only with Streaming HTML workflow (if your page exports an `onConnect()` handler) | Store data on the `data` object you have on the `page` property you get in your page route and page `onConnect()` handlers.` | [Kitten Kawaii](https://codeberg.org/aral/kitten-kawaii/src/branch/main/character_%5Bmood%5D_%5Bcolour%5D_%5Bname%5D.page.js#L157) |
| Session storage | Common to all pages within a client-side browser session (e.g., multiple pages open in tabs in the same browser. | On any handler that receives a `request` property | Store data on the `session` object you have on the `request` property you’re passed in your page route and page `onConnect()` handlers. | [Sessions tutorial](/tutorials/sessions/) |
| Route globals | Shares state between all handler calls of a route but is ephemeral (destroyed on server restart) | Any route | Create a file-level variable. | [Draw Together](https://codeberg.org/aral/draw-together/src/branch/main/index.page.js#L2) |
| Globals | Shares state within your application but is ephemeral | Anywhere | Create a global variable (e.g., `globalThis.myVariable`) | n/a |
| Persisted | Kept in the app database (persisted to disk; survives server restarts) | Anywhere | See [Database](#database) section, below. | [Persistence tutorial](/tutorials/persistence/) |

## Database

Kitten has an integrated [JSDB](https://codeberg.org/small-tech/jsdb) database that’s available from all your routes as `db`.

JSDB is a transparent, in-memory, streaming write-on-update JavaScript database for the Small Web that persists to a JavaScript transaction log.

Tables in JSDB are simply JavaScript objects or arrays and JSDB writes to plain old JavaScript files.

You can also create type-safe databases. To learn how to do so, read the [Database App Modules tutorial](/tutorials/database-app-modules/).

You can inspect and alter your database from the [Kitten’s interactive shell](#kitten-s-interactive-shell-repl) while your app/site is running.

You can also get information about the database and specific tables, delete the whole database and specific tables, and tail specific tables using the [command-line interface](#command-line-interface-cli) via the `db info [tableName]` (or just `db [tableName]`, which is a convenience alias), `db delete [tableName]` and `db tail <tableName>` commands.

To access the JavaScript source code of your database, follow the “Open app data folder” link in Kitten’s initial terminal output.

> 💡 Or, manually, open the relevant folder for your current site/app in the _~/share/small-tech.org/kitten/data/_ folder. Each project gets its own folder in there with a name based on the absolute path to your project on your disk (e.g., if a Kitten project is stored in _/var/home/aral/projects/my-project_, its database will be in a folder named _var.home.aral.projects.my-project_ in the main data folder.)

> 🔗 [Learn more about JSDB.](https://codeberg.org/small-tech/jsdb)


## Route parameters

You can include route parameters in your route paths by separating them with underscores and surrounding the parameter names in square brackets.

For example:

```text
manage_[token]_[domain].socket.js
```

Will create a WebSocket endpoint at:

```text
/manage/:token/:domain.socket
```

You can also intersperse path fragments with parameters:

```text
books_[id]_pages_[page].page.js
```

Will compile the Kitten page and make it available for HTTP GET requests at:

```text
/books/:id/pages/:page
```

So you can access the route via, say, _https://my.site/books/3/pages/10_.

You can also specify the same routes using folder structures. For example, the following directory structure will result in the same route as above:

```text
my-site
  ╰ books
     ╰ [id]
         ╰ pages
             ╰ [page].page.js
```

Note that you could also have set the name of the page to _index_[page].page.js_. Using just _[page].page.js_ for a parameterised index page is a shorthand.

You can decide which strategy to follow based on the structure of your app. If, for example, you could access not just the pages but the references and images of a book, it might make sense to use a folder structure:

```text
my-site
  ╰ books
     ╰ [id]
         ├ pages
         │   ╰ [page].page.js
         ├ references
         │   ╰ [reference].page.js
         ╰ images
             ╰ [image].page.js
```

You may, or may not find that easier to manage than:

```text
my-site
  ├ books_[id]_pages_[page].page.js
  ├ books_[id]_references_[reference].page.js
  ╰ books_[id]_images_[image].page.js
```

Kitten leaves the decision up to you.

### Optional parameters

To create an optional parameter, prefix your parameter name with `optional-` inside the square brackets.

For example, the following route:

```
delete
  ╰ index_[subdomain]_[optional-return-page].page.js
```

Will match both of the following paths:

- _/delete/my-lovely-subdomain/_
- _/delete/my-other-subdomain/troubleshooting/_

### The wildcard parameter

Finally, you can also use the special wildcard parameter to match any string:

```
my-site
 ╰ books_[any].page.js
```

The about route will match any path that begins with _/books/_.

> 💡 Under the hood, Kitten uses [Polka](https://github.com/lukeed/polka) for its routing and translates Kitten’s file system-based naming syntax to [Polka’s routing syntax](https://github.com/lukeed/polka/tree/next#patterns). 

## App Modules

When working on larger projects, you might end up with pages at varying levels within the site, all of which need to use the same global resource (for example, a layout template or a utility class).

In these situations, your import statements might start to get unruly.

Take the following example:

```
my-site
  ├ site.layout.js 
  ╰ books
     ╰ the-handmaids-tail
         ╰ pages
             ╰ cover.page.js   
```

Let’s say your _cover.page.js_ uses the site layout template.

Here’s what its import statement would look like:

```js
import Site from '../../../site.layout.js'

export default () => kitten.html`
  <${Site}>
    …
  </>
`
```

That’s both cumbersome to write and error-prone. (Especially when you consider that different pages at different levels of the hierarchy may want to use the same file. You don’t want to spend your day counting dots.)

Enter App Modules.

App Modules are special local Node modules that exist only in your app (not on npm). They live in the special _app_modules_ folder in your project and Kitten knows to ignore them when calculating its routes from the file structure of your project.

App Modules, like all Node modules, need a _package.json_ file but they can get away with specifying a subset of the information contained in ones for npm packages. And, like all Node modules, they need to be npm installed into your project (but from a local file path instead of from npmjs.org).

Finally, if you have type checking enabled in your projects, you should add an _index.d.ts_ file to your App Modules so your editor doesn’t complain when using the TypeScript Language Server (LSP).

Here’s how you’d make your site layout into an App Module:

1. Create the _app_modules_ directory and then a directory for your module:
   ```shell
   mkdir -p app_modules/site-layout
   ```

2. Add the files your module will need:
   ```
   my-site
     ├ app_modules
     │  ╰ site-layout 
     │     ├ site.layout.js
     │     ├ index.d.ts
     │     ╰ package.json
     ╰ books
        ╰ the-handmaids-tail
            ╰ pages
                ╰ cover.page.js  
   ```

3. In your package.json file, you need to specify three properties: `name`, `type`, and `main`:
   ```json
   {
     "name": "@app/site-layout",
     "type": "module",
     "main": "site.layout.js"
   }
   ```

4. Finally, if you have type checking enabled and you want to avoid type errors in your editor, your _index.d.ts_ file should look like this:
   ```ts
   import SiteLayout from './site.layout.js'
   export default SiteLayout
   ```

And that’s it.

All you need to do then is to install the App Module.

From the root of your project, install your module using its [local path](https://docs.npmjs.com/cli/v9/configuring-npm/package-json#local-paths):

```shell
npm install ./app_modules/site-layout
```

> 🪤 You should keep App Modules as simple as possible and they should not have their own dependencies. (We’re using them to simplify authoring, not to create a [monorepo](https://monorepo.tools/#what-is-a-monorepo).) That said, if you do end up having dependencies for your App Modules, remember to npm install them separately from within their own folders as npm will not *automagically* do that for you.

Finally, from _cover.page.js_ (and any other page, anywhere in your project) you can now import your layout using:

```js
import Site from '@app/site-layout'
```

That’s much better, isn’t it?

### Database app modules

Database app modules are special type of app module that Kitten expects to have the name _database_.

You are not limited to using the default, untyped database Kitten sets up for you.

The default database, like many of the beautiful defaults in Kitten, exists to make it easy to get started with Kitten and use it to build quick Small Web places, teaching programming, etc.

If, however, you’re designing a Small Web place that you will need to maintain for a long period of time and/or that you are going to collaborate with others on, it would make sense to create your own database and to make it type safe so you get both errors and code completion during development time.

You can create your own custom database using a special type of app module called the database app module.

Like any other app module, your database app module goes in the special _app_modules_ directory in the root of your project. What’s special is that the app module must be called _database_.

__To learn how to create your own database app module for your Small Web project, please read the [Database App Module tutorial](/tutorials/database-app-modules/).__

> 💡 For a real world example, see the [database app module](https://codeberg.org/domain/app/src/branch/main/app_modules/database) in [Domain](https://codeberg.org/domain/app) that wraps Kitten’s global [JSDB](https://codeberg.org/small-tech/jsdb) instance with type information and some utility methods.

## Static files

You do not have to do anything special to server static files with Kitten. It is, first and foremost, a web server, after all. Any file that’s in your project folder that isn’t a hidden file or in a hidden folder and isn’t a special Kitten file (e.g., .page.js, .socket.js, .post.js, etc.), will be served as a static file.

## Reserved routes

Kitten tries to be as minimally invasive into your site or app’s own namespace as possible. That said, there are certain routes that it does reserve. Some of these are part of the Small Web protocol and others are Kitten-specific.

### Reserved Kitten-specific routes

Kitten-specific routes are kept in the `🐱` namespace so as not to pollute your app/site’s own URI space.

These includes routes for default functionality that Kitten provides like the sign-in and sign-out routes as well as the settings interface:

- /🐱/settings (authenticated route)

- /🐱/sign-in

- /🐱/sign-out

### Reserved Small Web protocol routes

> 🚧 The protocol is currently under development and evolving rapidly.

The following routes are reserved as part of the Small Web protocol, which is namespaced by the Small Web emoji/logo (💕).

They are used to implement public-key authentication and manage the communication between Small Web instances and the Domain hosts that host them:

- /💕/id

- /💕/id/ssh

The Small Web protocol also defines a separate 🤖 (robot) namespace for the server itself (as opposed to the Small Web namespace which is for the _person_ who owns and controls the Small Web place).

Currently, that only contains the following route which returns the server’s public ED25519 signing key:

- /🤖/id

(This is used during server-to-server communication to ensure that requests actually originate from the server they purport to.)

__Kitten currently does not implement the full Small Web protocol.__ (The Small Web protocol is, itself, a work in progress.) It is currently up to apps to implement aspects of it in ways that make sense for them.

For example, [Place](https://codeberg.org/place/app) is in the process of implementing the following routes:

- /💕/follow
- /💕/profile

In time we’ll see whether or not it makes sense to pull in more base Small Web protocol implementations into Kitten proper.

## Kitten client-side libraries

Kitten comes with several client-side libraries which are automatically served at runtime that you can use in your sites/apps.

The [page tag](/tutorials/dynamic-pages/#the-page-tag) provides a high-level interface for including most of them on your pages. 

For example, to include htmx, you can add the following `page` tag to any HTML fragment that ends up on your page:

```html
<page htmx>
```

You can also include any of them in your sites/apps manually, just like any other client-side script, by knowing their paths:

In `/🐱/library/`:

| Library name | File name | Page tag attribute name | Version | Notes |
| ------------ | --------- | ------- | ------- | ----- |
| Kitten cryptography module | crypto-1.js | n/a (ES module) | ^1 | |
| [htmx](https://htmx.org)   | htmx-2.js | htmx | ^2 | Own fork. |
| [htmx WebSocket](https://github.com/bigskysoftware/htmx-extensions/blob/main/src/ws/README.md) | htmx-ws-2.js | ws | ^2 | |
| [htmx idiomorph extension](https://github.com/bigskysoftware/idiomorph/blob/main/README.md#htmx) | htmx-ideomorph-0.3.js | idiomorph | ^0.3.0 | |
| [Alpine.js](https://alpinejs.dev) | alpinejs-3.js | alpine | ^3 | |
| [Prism (CSS)](https://prismjs.com) | prism-1.css | n/a | ^1 | Used internally to syntax highlight Kitten error messages. Also see built-in [Highlight.js](https://github.com/valeriangalliat/markdown-it-highlightjs#readme) syntax highlighter. |
| [Water (CSS)](https://watercss.kognise.dev) | water-2.css | water | ^2 | |

The libraries also have minified and gzipped version (_.min.gz_) that are automatically used in production.

> 💡 The `-N` notation specifies the major version of the library. Since Kitten sites automatically update, if there is a major version update to a library, it will be adding alongside previous versions so as not to break existing sites. So if htmx 2.x.x comes out, for example, with breaking changes and we start supporting it, it will be added as `htmx-2.js`. All minor and patch updates will be automatically updated without changing the name of the include.

## 🐢 Kitten’s interactive shell (REPL)

### Introduction

Kitten has an interactive shell (REPL) that extends the default [Node.js REPL](https://nodejs.org/en/learn/command-line/how-to-use-the-nodejs-repl) with Kitten-specific features.

When working with Kitten interactively (i.e., during development), you can launch the shell using the <kbd>s</kbd> key from any Kitten instance running in your terminal.

You’ll know you’re in Kitten’s interactive shell when you see the following prompt:

```
🐱 💬
```

If you’ve deployed Kitten as a system service (daemon) using systemd, you can use the `kitten shell` command to connect to the daemon over a socket connection.

### Commands

In addition to executing JavaScript statements, Kitten also provides a number of useful custom commands you can use. These commands always begin with a dot.

For a full list of available commands, including [ones inherited from Node.js’s own REPL](https://nodejs.org/en/learn/command-line/how-to-use-the-nodejs-repl#dot-commands), use the `.help` command.

#### .ls

The `.ls` command shows you the keys of Kitten’s global `kitten` object – one of the most useful objects you can access in Kitten’s shell.

It is a convenience function for the more verbose `Object.keys(kitten)` JavaScript statement that you can also use for the same purpose.

#### .id

Shows your Small Web place ID (ed25519 public key) if you have one.

Otherwise, prompts you to use the [`.id create`](#id-create) command to create your Small Web place secret.

#### .id create

Creates your Small Web place secret (ed25519 private key, encoded as a lovely emoji string that you should keep in your password manager).

The output looks something like the following:

<pre>
   🆔 Identity

 » New ID created.

🔑 This is your secret:
</pre>

<div style='padding: 1em; border: 1px solid black; border-radius: 0.25em;'>🍮🐅🚲💕🦔🍑🥚🐻🌳🐹🐈🐴🥝🐩🪀🐼🐊📚🦎🐕🎓👽🌭🍑🎂🥑🧬🌳🔭👻🩰🎮</div>

<pre>
   Important: Please save your secret in your password manager.

   You need your secret to sign into your Small Web place and to communicate with your domain host.
   It’s the most important part of your Small Web identity.

   Public keys:

   ed25519:
   ee90d8b55011790192d76b925ff5db6bea9e4a979a1ccac784616431462825ab

   ssh:
   ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO6Q2LVQEXk… Kitten: person@localhost
</pre>

#### .settings

Shows the URL for accessing the Kitten Settings page of your app.

e.g., If you’re running it in development on the default port, it’ll return:

```
https://localhost/🐱/settings/
```

#### .sign-in

Shows the Sign In page URL for authenticating with your Kitten app.

e.g., If you’re running it in development on the default port, it’ll return:

```
https://localhost/💕/sign-in/
```

#### .sign-out

Shows the Sign Out page URL for signing out of your current session on your Kitten app.

e.g., If you’re running it in development on the default port, it’ll return:

```
https://localhost/💕/sign-out/
```

#### .stats

Shows statistics about your Kitten app (including statistics on event handlers and listeners) as well as any other Kitten processes that might be active on your machine, as well as basic statistics on all running processes and on the machine itself.

### Tutorial

Explore ways you can use Kitten’s shell (REPL) in the [Kitten’s Interactive Shell (REPL) tutorial](/tutorials/kitten-s-interactive-shell-repl).

## Command-line interface (CLI)

> 💡 Use `kitten help` to access command information from your terminal.
>
> To get detailed help on a specific command, use `kitten help <command name>`. For example, `kitten help serve`.
>
> You can also pass the `--help` or `-h` option to any command to get more detailed information about it. So `kitten serve --help` is equivalent to `kitten help serve`.

### Default command

#### serve

```shell
kitten [path to serve] [options]
kitten serve [path to serve] [options]
```

Serves a Kitten place.

> 💡 If do not specify a path to serve, the default directory (_./_) is assumed.

##### Options

- `--domain           `: Main domain name to serve site at. Defaults to hostname in production, localhost during development.
- `--aliases          `: Additional domains to respond to (each gets a TLS cert and 302 redirects to main domain). e.g., www.<hostname>
- `--port             `: The port to create the server on. Defaults to 443. Pass --port or --port=0 for random port.
- `--open             `: Open the page in the default browser.
- `--working-directory`: The working directory Kitten was launched from. Defaults to current directory  (default .)

> 💡 When running multiple Kitten instances on different ports to test peer-to-peer functionality during development, remember to use different domains for each instance (for example, `localhost` for one and `127.0.0.1:444` for the other) to ensure that sessions work correctly. This is because Kitten sessions make sure of cookies and [cookies do not provide isolation by port](https://www.rfc-editor.org/rfc/rfc6265#section-8.5). There is currently an issue open on [Auto Encrypt Localhost]() to [add IPs 127.0.0.2 - 127.0.0.4 to generated certificates](https://codeberg.org/small-tech/auto-encrypt-localhost/issues/6) so you can test up to four peers via the 127.0.0.1 - 127.0.0.4 loopback addresses (five, if you add localhost). If you have a need for more – perhaps for some sort of automated testing – please [open an issue here](https://codeberg.org/kitten/app/issues), explain your use case, and we’ll consider adding more.
>
> Also, please note that if you’re running an app at a random port, it will also get a random data directory assigned to it that’s valid for the time that the app is active. The next time it is run from a random port, it will have a different data directory so the data from the first run will not carry over. Only run apps at random ports if persistence of data between runs is not important to you. Small Web apps are not meant to be run from random ports and are meant to always be deployed in live environments from port 443 on a VPS or a dedicated device (like a single-board computer from home, for example). The random port feature exists to enable making local web apps (the equivalent of command-line applications or native apps) that run locally on a device. For an example of this, see the Markdown Preview utility in the _examples_ folder.

### Database commands

#### db (alias for db info)

```shell
kitten db [table name]
kitten db info [table name]
```

Shows database and table information.

#### db delete

```shell
kitten db delete [table name]
```

Deletes either the whole database or the table with the specified table name after asking you for confirmation.

#### db tail

```shell
kitten db tail <table name>
```

Shows a summary of the head (start) and tail (end) of your database table (remember that database tables in JSDB are append-only JavaScript logs) and starts to follow additions to it.

Press <kbd>Ctrl</kbd> <kbd>C</kbd> to exit as you would a regular shell `tail` command.

### Deployment of Kitten applications

#### deploy

```shell
kitten deploy <git HTTPS clone URL> [options]
```

Creates a Kitten place by deploying a Kitten project from a git repository as a systemd service.

##### Options

- `--domain           `: Main domain name to serve site at. Defaults to hostname in production, localhost during development.
- `--aliases          `: Additional domains to respond to (each gets a TLS cert and 302 redirects to main domain). e.g., www.<hostname>


#### run

```shell
kitten run <git HTTPS clone URL> [options]
```

Is the little brother of the `deploy` command. Runs a Kitten project from a git repository locally as a regular process.

It’s options are the same as that of the default `serve` command.

### Kitten daemon (systemd service) commands

#### status

```shell
kitten status
```

Shows Kitten systemd service status.

#### logs

```shell
kitten logs [options]
```

Tails (follows) the Kitten systemd service logs using journald.

##### Options

* `--since`: Time span to show logs for. For example, `6 hours ago`, `yesterday`, etc. (defaults to `1 hour ago`).

#### start

```shell
kitten start
```

Starts the Kitten systemd service.

#### stop

```shell
kitten stop
```

Stops the Kitten systemd service.

#### enable

```shell
kitten enable
```

Enables the Kitten systemd service (so it auto starts on boot and if the process exits)

#### disable

```shell
kitten disable
```

Disables the Kitten systemd service (so it no longer auto starts on boot, etc.)

### General commands

#### version (aliases: --version and -v)

```shell
kitten version
kitten --version
kitten -v
```

Displays the Kitten version, made up of:

- Date of the build (the Kitten’s birthday)
- The git commit hash of the build (as represented by the RGB colour equivalent of the hex value; the Kitten’s favourite colour)
- The API version (the major semver component from _package.json_.)

> 🚧 Eventually, you will be able to tell Kitten what API version your app is written for and it will only auto-update to the latest release in that API version.

#### help (aliases: --help and -h)

```shell
kitten help
kitten help <command name>
kitten <command name> --help
kitten <command name> -h
```

Use `kitten help` to access command information from your terminal.

To get detailed help on a specific command, use `kitten help <command name>`. For example, `kitten help serve`.

You can also pass the `--help` or `-h` option to any command to get more detailed information about it. So `kitten serve --help` is equivalent to `kitten help serve`.

#### update

```shell
kitten update
kitten update <API version>
```

Use `kitten update` to update (upgrade or downgrade) your currently installed version of Kitten.

When you run the command without supplying the optional `API version` argument, Kitten will attempt to update to the more recent publicly-available Kitten package from [kittens.small-web.org](https://kittens.small-web.org). If your version of Kitten is more recent than the most recent publicly-available Kitten package, Kitten will prompt you to ask if you want to downgrade your installation.

If you supply the optional `API version` argument, the update will check for the latest version for the given API version. If such an API version does not exist, you will get an error. If your version of Kitten is already on that API version but is a more recent build, you will be prompted whether you want to downgrade your installation.

> 💡 The only time the downgrade prompt should show is if you’re running a local development build that is more recent that the most recent publicly-available Kitten package. This is useful if you want to quickly test a publicly-available package during development. You can, of course, always install the latest development version by running the `./install` script as usual.

#### uninstall

```shell
kitten uninstall
```

Uninstalls Kittens and removes all Kitten data after asking for confirmation.

## Troubleshooting

Here are some edge cases you might encounter (because others have encountered them) and what you can do about it:

### Linux: Untrusted localhost TLS certificates in Chrom(ium)

If at some point Chrom(ium) starts complaining that your development-time localhost certificates are untrusted under Linux, check that you haven’t accidentally installed a copy of `ca-certificates` and/or `p11-kit` in your unprivileged account using a third-party package manager like Homebrew (`brew`).

If this is the issue, you should:

1. Remove the `ca-certificates` and `p11-kit` packages from Homebrew (you will also have to remove any packages that depend on them. e.g., python, OpenSSL, etc.)
2. Restart your machine.
3. Run `sudo update-ca-trust extract`
4. Uninstall Kitten (`kitten uninstall`)
5. Reinstall Kitten [via the one-line installation command](/#install) or from your working copy of the source code (`./install`)

That should do it.

To check if your system trust store is set up correctly, you can run the following command:

```shell
trust list --filter=ca-anchors | grep Localhost
```

And you should see output that resembles the following:

```
label: Localhost Certificate Authority for aral at dev.ar.al
```

## Building Kitten

While developing Kitten, it’s best practice to run the `install` script and use the `kitten` command to run your installed build.

```shell
./install
```

A typical run of the install commands takes about half a second on a modern computer so it should not impact your development velocity negatively.

> 💡 In order to keep the development build/install process as quick as possible, dependencies are not updated unless you specifically request an `npm install` by passing the `--npm` flag:
> 
> ```
> ./install --npm
> ```
> 
> (However, an npm install will be carried out if this is the first time you’re building/installing Kitten locally.)

There is a separate `build` command (called internally by the install script) and if you use that, you will find the distribution under the _dist/_ folder.

To run Kitten from the distribution folder, use the following syntax:

```shell
dist/kitten [path to serve]
```

> 💡 Kitten’s build + install process (what happens when you run the _install_ script) takes less than half a second on a modern computer and has the additional benefit of informing you of compile-time errors. It’s highly recommended you don’t run build by itself or run from the dist folder directly unless you have specific reason to.

## Debugging

To run Kitten with the Node debugger active (equivalent of launching Node.js using `node --inspect`), start Kitten using:

```shell
INSPECT=true kitten
```

> 💡 If you use VSCodium, you can add breakpoints in your code and attach to the process using the Attach command in the Run and Debug panel.

> 💡 In Chromium, you can use the Node debugger (enter `chrome://inspect` in address bar → select ‘Open dedicated DevTools for Node’).

## Profiling

You can see profiling information provided by Kitten’s `console` mixin methods `profileTime()` and `profileTimeEnd()` by passing the `PROFILE=true` environment variable:

e.g.,

```shell
PROFILE=true kitten examples/streamiverse
```

### Flame Graphs

To narrow down performance issues by time spent on the stack, you can have Kitten generate a [flame graph](https://www.brendangregg.com/flamegraphs.html) by setting the `FLAME_GRAPH` environment variable to `true`.

e.g.,

```shell
FLAME_GRAPH=true kitten examples/streamiverse
```

**This will start your project in production mode** and globally install and use the [0x node module](https://github.com/davidmarkclements/0x#readme) to capture profiling information and automatically launch a browser when the process exits to show you the flame graph.

> 💡 Flame graphs can only be generated in production mode as the 0x module is not compatible with Node’s cluster module and the latter is what we use to implement Kitten’s process manager in development mode. 

## Testing

Kitten’s tests are a work-in-progress and steadily improving.

There are three types of tests in Kitten:

### Unit tests

Unit tests are written in [Tape With Promises](https://github.com/small-tech/tape-with-promises), run using [ESM Tape Runner](https://github.com/small-tech/esm-tape-runner), and displayed using [Tap Monkey](https://codeberg.org/small-tech/tap-monkey/).

Coverage is provided by [c8](https://github.com/bcoe/c8).

Run tests:

```shell
npm -s run unit-tests
```

Run coverage:

```shell
npm run -s coverage
```

> 💡️ The `-s` just silences the npm logs for cleaner output.

> 💡️ Unlike regression tests and end-to-end tests (see below), unit tests are run on the Kitten source code instead of the built Kitten bundle.

### Regression tests

Regression tests are written with the same stack as Kitten’s unit tests but use the `Kitten` and `Browser` test helpers to test initial output from test fixtures (the example apps).

These do not test client-side interactions, for that, please see end-to-end tests, below.

(In the future, these tests might be removed in favour of only keeping the end-to-end tests as they test similar things. The only advantage of the regression tests is that they are faster to run as they test using JSDOM instead of actual browsers.)

Run tests:

```shell
npm run -s regression-tests
```

> 💡 Regression tests initially perform a Kitten install to ensure you are testing with the latest Kitten bundle. If you do not want this, run the `regression-tests-without-install` tast instead of the `regerssion-tests` task.

## End-to-end tests

End-to-end tests use [Playwright](), along with our custom `Kitten` helper for controlling the Kitten server to test Kitten’s behaviour using actual browser engines (Safari/Webkit, Firefox/Gecko, Chromium/Blink).

Run tests:

```shell
npm run -s end-to-end-tests
```

> 💡 Like regression tests, end-to-end tests perform Kitten install prior to running the tests. To skip this, run the `end-to-end-tests-without-install` task.

> 💡 By default, tests run in headless browsers. You can also run the tests in Chromium with the interface showing so you can see the state of the web interface at every state of the tests. To run the tests this way, execute the `end-to-end-tests-in-browser` task. There is also a `end-to-end-test-in-browser-without-install` task you can use to skip the install Kitten install.

## All tests

To perform a Kitten install and run all tests:

```shell
npm -s test
```

This will run unit, regression, and end-to-end tests.

## Deployment (of Kitten itself)

To build and deploy a new Kitten package to [kittens.small-web.org](https://kittens.small-web.org),
run the deploy script:

```shell
./deploy
```

> 💡You will need to add the secret deployment token to your system in order to deploy. If you have deployment rights for Kitten, follow the instructions available at https://kittens.small-web.org/settings.

## Core dependencies

| Dependency                                                         | Purpose                                                             |
| ------------------------------------------------------------------ | ------------------------------------------------------------------- |
| [@small-tech/https](https://codeberg.org/small-tech/https)           | Drop-in replacement for Node’s native https module with automatic TLS for development and production using [@small-tech/auto-encrypt ](https://github.com/small-tech/auto-encrypt) and [@small-tech/auto-encrypt-localhost](https://github.com/small-tech/auto-encrypt-localhost). |
| [@small-tech/jsdb](https://codeberg.org/small-tech/jsdb)             | Zero-dependency, transparent, in-memory, streaming write-on-update JavaScript database that persists to JavaScript transaction logs. |
| [Polka@next](https://github.com/lukeed/polka)                      | Native HTTP server with added support for routing, middleware, and sub-applications. Polka uses [Trouter](https://github.com/lukeed/trouter) as its router. |
| [tinyws](https://github.com/tinyhttp/tinyws)                       | WebSocket middleware for Node.js based on ws. |
| [xhtm](https://github.com/dy/xhtm)                                 | XHTM is alternative implementation of [HTM](https://github.com/developit/htm) without HTM-specific limitations. Low-level machinery is rejected in favor of readability and better HTML support.                                                                                   |
| [hyperscript-to-html-string](https://codeberg.org/small-tech/hyperscript-to-html-string)                        | Render xhtm/Hyperscript to HTML strings, without virtual DOM. |
| [sanitize-html](https://github.com/apostrophecms/sanitize-html)    | Clean up untrusted HTML, preserving whitelisted elements and whitelisted attributes on a per-element basis. Built on htmlparser2 for speed and tolerance |
| [node-git-server](https://github.com/gabrielcsapo/node-git-server) | Git server for hosting your source code. Used in deployments. |
| [isomorphic-git](https://isomorphic-git.org/)                      | Git client used in deployments on development and for handling auto-updates on production. |
| [sade](https://github.com/lukeed/sade)                             | A small command-line interface (CLI) framework that uses [mri](https://github.com/lukeed/mri) for its argument parsing. |
| [noble-ed25519](https://github.com/paulmillr/noble-ed25519)        | Cryptography: ed25519 key generation. |
| [ed25519-keygen](https://github.com/paulmillr/ed25519-keygen)      | Cryptography: SSH key generation from ed25519 key material.  |

Kitten’s renderer is built upon [xhtm](https://github.com/dy/xhtm) and [vhtml](https://github.com/developit/vhtml) although both of those libraries have been inlined and extended.

For an automatically-generated full list of modules and contributors, please see [CONTRIBUTORS.md](https://codeberg.org/kitten/app/src/branch/main/CONTRIBUTORS.md).

## Cryptographic properties
 
**(The functionality described in this section is currently being developed both in Domain and Kitten.)**

### Overview

The security (and privacy) of [Domain](https://codeberg.org/domain/app)/Kitten are based on a 32-byte cryptographically random secret string that only the person who owns/controls a domain knows.

This is basically a Base256-encoded ed25519 secret key where the Base256 alphabet is a set of curated emoji surrogate pairs without any special modifiers chosen mainly from the animals, plants, and food groups with some exceptions (to avoid common phobias or triggers, etc.) that we call KittenMoji.

```
🐵🐒🦍🦧🐶🐕🦮🐩🐺🦊🦝🐱🐈🦁🐯🐅
🐆🐴🧮🦄🦓🦌🦬🐮🐂🐃🐄🐷🐖🐗🐽🐏
🐑🐐🐪🐫🦙🦒🐘🦣🦏🦛🐭🐁🐀🐹🐰🐇
🎈🦫🦔🦇🐻🐨🐼🦥🦦🦨🦘🦡🐾🦃🎹🐓
🐣🐤🐥🐦🐧💕🦅🦆🦢🦉🦤🪶🦩🦚🦜🚲
🐊🐢🦎📚🐉🦕🦖🐳🐋🐬🦭🐟🐠🐡🦈🐙
🐚🐌🦋🐛🐜🐝🪲🐞🦗🎭🎁🧬🪱🦠💐🌸
🎠🌈🌹🧣🌺🌻🌼🌷🌱🪴🌲🌳🌴🌵🌾🌿
🎤🍀🍁🪺👽🍇🍈🍉🍊🍋🍌🍍🥭🍎🍏🍐
🍑🍒🍓🫐🥝🍅🫒🥥🥑🍆🥔🥕🌽🧸🫑🥒
🥬🥦🧄🧅🍄🥜🌰🍞🥐🥖💩🥨🥯🥞🧇🧀
🎶🏸🎾🎨🍔🔭🍕🌭🥪🌮🌯😸📷🌜🥚🚂
🛼🚁👾👻🥗🍿🧩🖖🥫🎸🍘🍙🍚🃏🍜🍝
🍠🍢🍣🍤🍥🥮🍡🥟🥠🩰🦀🦞🦐🦑🎡🍦
🍧🍨🍩🍪🎂🍰🧁🥧🍫🍬🍭🍮🎓🍼🎮🛹
🫖🌍🌎🌏🧭🌠🪐🪀🧵🧶🧋🎉🪁🙈🙉🙊
```

> 🐱 The KittenMoji standard was frozen in time on Friday, November 25th, 2022.
>
> The string created by joining [the list of the full KittenMoji alphabet](https://codeberg.org/kitten/app/src/commit/f2258bdb860bc78d439a31da9e589e747ef608fc/src/lib/KittenMoji.js#L89) has the following hash: 
 `d9b1533de36a75c9e47f1713aada4ff332918ff0d3cada5b645ca84d73dd50d6`.

When setting up a Small Web app via Domain, this key is generated in the person’s browser, on their own computer, and is never communicated to either the Domain instance or the Kitten app being installed. Instead the ed25519 public key is sent to both and signed token authentication is used when the server needs to verify the owner’s identity (e.g., before allowing access to the administration area).

The expected/encouraged behaviour is for the person to store this secret in their password manager of choice.

From this key material, we derive SSH keys and the person’s server is set up to allow SSH access via this key.

Kitten running on a development machine can also recreate these SSH keys and configure the person’s SSH access to their server when the secret key is provided.

The person’s ed25519 public key is used as their identity within the system and enables their Small Web place (Kitten app) to communicate with the Domain instance for administrative reasons (e.g., to cancel hosting, etc.)

### Threat model

The audience for Domain/Kitten and the Small Web in general is everyday people who use technology as an everyday thing and want privacy by default in the systems they use.

If you are an activist, etc., who might be specifically targetted by nation states, etc., please do not use this system as it does not protect against, for example, infiltration of hosting providers by nation state actors.

Given that the secret key material is generated in a web app, you must trust that the code being served by the server is what you expect it to be. Currently, there is no validation mechanism for this, although this is on the roadmap going forward (via, for example, a browser extension). This is not a problem unique to web apps, given that native apps are commonly dynamically built by app stores (at which point a nation state actor could inject malicious code) and given the lack of verifiable builds in mainstream supply chains.

You can, however, overcome this limitation by hosting Domain yourself, on your own hardware (e.g., a single-board computer like a Raspberry Pi).

The other thing to be aware of is that the security of the system is based on your secret key remaining secret and, initially at least, there will not be a way to change this secret key. (On the longer roadmap, it would be nice to provide a means of changing it but this is not a trivial process, especially if encryption keys have been derived from it and encrypted messages exist within a Kitten app).

Finally, as of September 2024 when this was last updated, the major browser engines do not yet all support [ed25519](https://caniuse.com/mdn-api_subtlecrypto_sign_ed25519)/[x25519](https://caniuse.com/mdn-api_subtlecrypto_derivekey_x25519) in the Web Crypto API. Specifically, while Apple (Safari) and Mozilla (Firefox) have implemented it, Google (Chrome/Chromium) hasn’t. This means that we must use a JavaScript library to handle the cryptography ad store the secret key in local storage on the client. Therefore, if a Kitten app is compromised via a [Cross-Site Scripting (XSS) attack](https://owasp.org/www-community/attacks/xss/), the secret can be obtained. Once Chrome and Chromium-based browsers implement the feature by default (it is currently behind a flag), Kitten will migrate to using the Web Crypto API and the secret key will be stored in an inextractable manner that is safe from exposure via XSS.

## Install Kitten on Windows under WSL 2

> 🙀 __Windows is an ad-infested and surveillance-ridden dumpster fire of an operating system and you are putting both yourself and others at risk by using it.__

Kitten is not supported on Windows although you might be able to get it running under Windows 10 &amp; 11 under WSL 2 using the instructions here.

_Please do not open issues for Windows-specific issues as they will not be fixed._

Note following caveats:

1. Requires WSL 2 __(will not work with WSL 1)__.
2. You must manually install Kitten’s local development-time certificate authority in your Windows browsers.

If you haven’t installed Kitten on Windows or used WSL 2 before, the instructions, below, take you through the whole process.

> 🚨 The process – which is not guaranteed to work or continue working (again, Kitten is not supported on Windows and as part of that, it is not tested on Windows) – is not as seamless as it is on Linux and macOS.
>
> You must first install WSL 2 and you must manually add Kitten’s local development-time certificate authority to the trust stores of your Windows browsers to avoid certificate errors:


### Instructions

1. __Open up a Windows Powershell tab in Terminal App.__

    > 🪤 Windows comes with a couple of terminal apps and shells. Make sure you use the exact pair mentioned above.

2. __Install WSL 2:__

      ```shell
      wsl --install
      ```

    This will install WSL 2 and Ubuntu. It will ask you to choose an account name and set a password and then you will be running Ubuntu under WSL2.

    __Kitten only works with WSL 2 using Windows Terminal and Windows Powershell__ ([the `sysctl` command used by the installer fails on systems with WSL 1](https://codeberg.org/kitten/app/issues/60)).

    To make sure your Linux container is running under WSL 2 (and not 1) before continuing, run:

      ```shell
      uname -a
      ```

    On my system, that gives me the following system information:

      ```
      Linux aral-win11 5.15.90.1-microsoft-standard-WSL2 #1 SMP Fri Jan 27 02:56:13 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
      ```

    Note that the string `WSL2` is contained in the output. If you don’t see that, you’ll have to either create a new container using WSL 2 or update your current container to use WSL 2.

      > 💡 __For best results, please make sure you’re running the latest Ubuntu version__ that’s installed automatically by wsl when you run the `wsl --install` command.
      > 
      > (If you’re running a very old version of Ubuntu, it may not have the new Let’s Encrypt certificate authority in its trust store and the Kitten installer may fail with a certificate error. If this happens, please either upgrade your version of Ubuntu or try tunning `sudo dpkg-reconfigure ca-certificates` to download the latest list of trusted certificate authorities into the system store.)

3. __Install Kitten:__

      ```shell
      wget -qO- https://codeberg.org/kitten/app/raw/install | bash
      ```

      (Enter the password you set up for your Ubuntu installation under WSL 2 when asked.)

4. __Source your _.profile_ to add Kitten’s binary to your path:__

      ```shell
      source .profile
      ```

      (Kitten is now installed properly but Ubuntu doesn’t add the _~/.local/bin_ folder that Kitten puts its binary into to your system path until that path exists on your computer. Instead of sourcing the _.profile_, you can also log out and log back in.)

5. __Test the Kitten binary.__

      ```shell
      kitten --version
      ```

      You should see Kitten launch and the version get displayed.

      > 💡If Kitten fails to launch, [please open an issue](https://codeberg.org/kitten/app/issues) and try to give as much information about what went wrong as you can. Copy and paste any error messages you see. Also run `uname -a` and `lsb_release --all` and paste the output from those commands into your issue.

6. __Create your first project and run Kitten.__

      > ❣️ __Don’t skip this step!__ The first time Kitten runs, it creates your local development-time certificate authority (CA) and TLS certificate. You will need the CA to install in your Windows browsers in the next section.
      
      ```shell
      mkdir hello-kitten
      cd hello-kitten
      echo 'Hello, Kitten!' > index.html
    kitten
    ```

    Kitten should launch and start serving your site.

    However, if you open a web browser under Windows (e.g., the default Edge browser), you will see a `NET::ERR_CERT_AUTHORITY_INVALID` security error warning you that your connection isn't private.

    This is because Kitten is running under Ubuntu and your browser is running under Windows. While Kitten can (and does) update your Linux system trust store to accept its local development certificates, it cannot do that for your Windows machine because it doesn’t even know that it exists.

    So, on Windows, you have to manually install Kitten’s certificate authority in your Windows browsers.
    
    The next section takes you through doing that.

### Manually install the certificate authority in your Windows browsers.

For browsers you have installed under Windows to accept the TLS certificates created by Kitten, you must install its certificate authority into your browsers. (Remember that Kitten is running under Linux and doesn’t know anything about your Windows environment so it can’t do it for you.)

For example, if you’re using Microsoft Edge:

1. Go to `edge://settings/privacy`, scroll down to the Security section, and select “Manage certificates.”

    ![Screenshot of Settings page in Microsoft Edge.](https://codeberg.org/attachments/c20bb364-410d-4c6c-a474-9d4a7febe672)

2. In the resulting “Certificates” modal, select the _Trusted Root Certificate Authorities_ tab and press the _Import…_ button.

    ![Screenshot of the Certificates modal showing the empty Personal tab selected](https://codeberg.org/kitten/app/attachments/0bc37198-fcc4-404f-84ea-0c83164646f2)

3. In the resulting _Certificate Import Wizard_, press _Next_ to pass from the welcome screen to the _File to Import_ step and press the *Browse…* button.

    ![Screenshot of the File to Import stage of the Certificate Import Wizard with an empty File name textbox and the Browse… button mentioned in the text.](https://codeberg.org/attachments/e9433db5-2675-48e9-ade9-a253fa2761d2)

4. Make sure the file extensions drop-down is set to show all extensions and select *Linux → Ubuntu → home → (your home directory) → .local → share → small-tech.org → kitten → tls → local → auto-encrypt-localhost-CA.pem*.

    ![Screenshot of the file dialogue showing the auto-encrypt-localhost-CA.pem certificate selected.](https://codeberg.org/attachments/f6e6b3a9-e176-47cf-b8cd-cee35f7b97a2)

5. In the next step, make sure “Place all certificates in the following store” is selected with “Trusted Root Certification Authorities” set.

    ![Screenshot showing the Certificate Import Wizard with the settings mentioned in the step applied.](https://codeberg.org/attachments/95d237ba-895f-4b48-b3d2-d08f9a9d7a10)

6. Press _Next_ and, on the final screen, press _Finish._

    ![Screenshot of Certification Import Wizard: Completing the Certificate Import Wizard. The certificate will be imported after you click Finish. (Contains a summary of the selected certificate settings.)](https://codeberg.org/attachments/ddcab715-ce1f-47fa-9dff-bf09ca545399)

7. After the wizard closes, you will see a Security Warning telling you that you are about to install a root certificate. Select _Yes_ to continue.

    ![Screenshot of Security Warning modal: “You are about to install a certificate from a certification authority (CA) claiming to represent: Localhost Certificate Authority for aral at aral-win11. Windows cannot validate that the certificate is actually from …”](https://codeberg.org/attachments/75133f45-d185-41f8-a726-994ca4c9728e)

Finally, restart Edge and you should be able to hit _https://localhost_ without certificate errors.

Now that you’ve successfully installed Kitten on Windows under WSL 2, you can continue learning about Kitten by following the [Getting Started](#getting-started) guide and tutorials.

## Questions?

Contact <a href="https://mastodon.ar.al/@aral" rel="me">Aral</a> on the fediverse.
