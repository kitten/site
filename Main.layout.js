import Header from './Header.fragment.js'
import Footer from './Footer.fragment.js'
import Styles from './Main.fragment.css'

export default ({ title, pageId, skipToContentId = 'main', SLOT, ...props }) => {
  return kitten.html`
    <page css syntax-highlighter>
    <${Header} pageId=${pageId} skipToContentId=${skipToContentId} />
    <main id='main' ...${props}>
      ${SLOT}
    </main>
    <${Footer} />
    <${Styles} />
  `
}
