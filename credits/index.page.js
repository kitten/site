// @ts-check

/**
  Retrieves the automatically-generated CONTRIBUTORS.md file from
  Kitten‘s source code repository on Codeberg and renders it.
*/

import kitten from '@small-web/kitten'
import MainLayout from '../Main.layout.js'
let contributorsHtml

export default async () => {
  // Lazily get the contributors page from Codeberg.
  if (contributorsHtml === undefined) {
    // Get the latest Contibutors.md file from the source control
    // repository and render it.
    let contributorsMarkdown
    try {
      contributorsMarkdown = await (await fetch('https://codeberg.org/kitten/app/raw/branch/main/CONTRIBUTORS.md')).text()
    } catch (error) {
      console.error('❌ Could not fetch CONTRIBUTORS.md from Kitten git repository on Codeberg.')
      return kitten.html`
        <${MainLayout} pageId=credits>
          <h2>Oops!</h2>
          <div class='error'>
            <markdown>
              ❌ __Could not load credits from Codeberg.__
        
              This page fetches the automatically-generated [CONTRIBUTORS.md](https://codeberg.org/kitten/app/src/branch/main/CONTRIBUTORS.md) file from [Kitten’s source code repository](https://codeberg.org/kitten/app) on [Codeberg](https://codeberg.org).

              Please [check Codeberg’s status](https://status.codeberg.eu/status/codeberg) and reload the page if all seems fine on their end to try again.

              _If you are still having trouble, please contact <a href="https://mastodon.ar.al/@aral" rel="me">Aral</a> on the fediverse._
            </markdown>
          </div>
        </>
      `
    }
    console.log(contributorsMarkdown)
    contributorsHtml = globalThis.kitten.md.render(contributorsMarkdown)

    // Accessibility: replace the empty th tag with td.
    // (Best practice. No impact on WCAG AA compliance.)
    contributorsHtml = contributorsHtml.replaceAll(/<th><\/th>/g, '<td></td>')
  }
  return kitten.html`
    <${MainLayout} pageId=credits>
      ${[contributorsHtml]}
      <style>
        table { table-layout: inherit; }
        td    { vertical-align: middle; }
        img.avatar {
          width: inherit;
          max-width: 10vw;
          /* To solve phantom space under image. */
          display: block;
        }
      </style>
    </>
  `
}
