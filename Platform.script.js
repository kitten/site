export default class Platform {
  static LINUX = 'Linux'
  static MAC = 'Macintosh'
  static WINDOWS = 'Windows'
  static OTHER = 'other-unsupported-platform'
  
  platform = null
  detected = null
  platforms = [Platform.LINUX, Platform.MAC, Platform.WINDOWS]
  platformNames = {
    [Platform.LINUX]: 'Linux',
    [Platform.MAC]: 'macOS',
    [Platform.WINDOWS]: 'Windows',
    [Platform.OTHER]: 'Other'
  }

  static fromRequest (request) {
    return new Platform({ request })
  }

  static withValue (value) {
    return new Platform({ value })
  }

  constructor ({ request, value }) {
    if (value !== undefined) {
      // Direct value passed. Use that instead of detecting from request.
      this.platform = value
      this.detected = false
      return
    }

    // Attempt to detect the platform from the user agent string in the request header.
    this.detected = true
    this.userAgentString = request.headers['user-agent']
    this.platforms.forEach(platform => {
      if (this.userAgentString.includes(platform)) {
        this.platform = platform
      }
    })
    if (this.platform === null) {
      this.platform = Platform.OTHER
    }
  }

  get name () {
    return this.platformNames[this.platform]
  }

  get key () {
    return this.platform
  }

  is (platform) {
    return this.platform === platform
  }
}
