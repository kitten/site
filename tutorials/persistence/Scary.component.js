import kitten from '@small-web/kitten'

const random = () =>
  // Take the sine of a random angle between 45° and 135° (-0.707 and 0.707)
  // (See it visually at https://www.mathsisfun.com/algebra/trig-interactive-unit-circle.html)
  Math.cos(Math.random() * 90 + 45) 
  // Add a larger random twitch every now and then (~0.05% of the time)
  // to keep things interesting.
  + (Math.random() > 0.95 ? Math.cos(Math.random() * 90 + 45) * 5 : 0)

/** This embeds a subset of the Creepster font. If you want to be able to write any text, you should embed a more comprehensive version. */
export default ({
  text = 'SCARY',
  fontSize = '1em',
  verticalAlign = 'inherit'
}) => {
  const lettersToAnimate = [...text]
  return kitten.html`
    <span class='Scary'>${
      lettersToAnimate.map((letter, index) => letter === ' ' ? '&nbsp;' : kitten.html`
        <span class='Scary animation${index}'>${letter}</span>
      `)
    }
      <style>
        @font-face {
          /* 
            Subset of Creepser font by Sideshow (OFL license)

            The web font includes the letters we’re using in the scary message
            and so it’s only 8kb in size. 

            Font: https://www.1001fonts.com/creepster-font.html
            Online subsetting tool: https://everythingfonts.com/subsetter
            Online TTF to WOFF2 convertor: https://cloudconvert.com/ttf-to-woff2
          */
          font-family: 'scary';
          font-weight: normal;
          font-style: normal;
          src: url(data:font/woff2;base64,d09GMgABAAAAABO8ABAAAAAAKJgAABNeAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP0ZGVE0cG4F6HDAGYACCeggEEQgKtUilZQsoAAE2AiQDTAQgBZBSB0gMBxt3H6OihpNa7GR/kWAbotYPbQEtMQyEvnKASHGxHCAvlNdvgHNnaTtZ58IvIPDPqxGSzA5P2/x3UdxBH3HUmYABh1iF0VNo3ab7iOJcaM+/uS79Lip0659dRNWw9ezdPyFJk3J5jAIrEBKJkFlIVHYo9O679jhCh+zmNKm9KdfYu42TQkofCgcDJPnhSmfufVhKeHePDHSS/WEuekOY65TntGlPsv3IVbCqftJUyXDzeP9rqVKd3aFZWtvUgkAADiG7b8+n+bsqd+d2WkkuctpFcruU5tpoSsOVELcSlKDAABIWQAJoIMkEMBgb1bY9RiEXxRX2zz7Eq8gsnSKecJqe/PhpAAAIAOCtjB9WAwA8zWavAQD4oCw9CQKAiKAYfFrVEACS28hQD+CgAGKMvOwP3XQO9AASOO2EgvctUu+l+h6KQb4KIX2WWm+rCQS8/0bw5mR/g+rfyDrynoIzMBTYolEaiySxJ5klfNhlQxCdkCssB4PAFVmP4AwMB9G+CEH7m/3yH63fShQUVkMN6Q1yCq2vTmOv1g47se9CJNez3fcH8FsxALYNb0K/XLhqH1y5HLAcAlBnw4qrRh8oxWsZz3UcsGn2vjtc+I+E1XzRJNJNGNZ7YEho6LQO1ZxRkGF7GM+tUq0mASEt0uH69HI1Gvj+pP7oh/uhfqDv63v67r4FcudBS3am4D8rvBHiTOAOsMaWSwNyDQDRBiMGQLJjXhH/gYcmvtZrubv/Jy1XWskx1qOKHTcSY9N2eAwM2o8TzMVPf9RENYur9PXCJP9y9a/x6HlOnn+49S3eunfLxRjbdS0gssQmRAUnX9p+hwAGp+0M19Tw3FtD7xKxwllui8+NNGH7bZZKwjGwArhDRsOIjUI8Mu+pVUdxq4HwskCZiLjg7QGltQEyOB41kSkg+RFr+hWUwXTBPWABLFEiwpBjSel1qSoSo33Zqo+5RWt54tIoLHXCNc6lpPBQVjJrFrxh3y7xM9h15gMPfr2UcvoBkVST0tYTJYrbZfBa4VTEXlHT7LjAT2RpUaMfsI0SPGGQggnSasc5Ul5fX7u2qtvy4Njq3WIfTdFXNGqFBdYKKDXkbZJuq3JJFuCXpcIswKoAsHiJ7iKTQgmYYJlz5J7MozDUs6Yz1eUrL263Zj9ll1A8vh9pU9HjMTYueCUjlSU1a5Os6ltTaKOJl0eb/k/PAkur0moPznXpq9Z51J9BADH0DY4K2EWN7k8kBZPSRBqK3Mo8UtwUlEANJp+LJJJ/NrGEOKOXo1VZf7sdU+AzKB/7IzbtyYHSOMsMApQxPNi+kWKjXP3YWiP5JIRb5Om6SEOo1deGKeyTtcqsh8ndzb/Sb9E2nwy8SxYcePVYE4AI4UTNJJr+mBrfDffsP4/mXcIJXy2MX4oSy9y2dyDPBfsTbOqL4ME1hg2HCeMfPymgxNYW0tozhvcV8gRRS0rfZhk1YY2BrtEI0m1w+jZWEtajCVZgIxRLzKRBbVr1C082dZlEKqL2Sov1RIMIHd9TnIKMcoEd/YJb63PjOvf1OJdSaPw+J70pR6PHE8HGkrTPxmM3ugrIkV5Yt11/aWsAwKhentBajMwVgv/Ytma+xWUyqNGceNLWagJem23NsmKI6W6TBgHCgyDNI2ZWVsznnVFTWTjV9gEvQKHVLJ80G8w767rZIyU5wydmKVggkYGqMvExK3appcR7IkN9JSVlIyR0v6y3sd9o2YUFAR66EKVzoj/BYC/pfsjqiH7oJfi6YHcTIEQAqKSx07b/lP1YqbZyxafaUSh3SbG96CxVytvx1tsMPHRnpQ2Cgo+ozk4xRBgilW0bZZPCOxqx0Y7URMgUSyseaqKKVgVs4HYZOcY1rS4TNU+80NQPvtuoJWYk854C9Hja510nX83ag/TTRk8LTk1wpBGmfC4dBjhSfsa8vyUDxnFVgsV6NHRXvXl5bmTMye0hvepwLxzBSE//JniomIPg0GLFxqWibzgImAz7KvZ95ggINoYSICmPT1gDE/lEf2alSuWV5FFHyzHR7rgNd8XZLe+aIIO5LOQ1EDB0DQxRgF4Ogf3VaOCDDh7xvME1KF1mCN0QEq1KPbUpo0Qq2SzVVw4iiS98vcCQlKxPx8sqpdtj3GYZVqIXN3vtlZsmcpasRZS5P4ZxB6GIqJA5SAr+MrXLeOqSXxRwnwidWwh+y+iNjSQDSDIykgI/qxn/Ip+IhcxsC6S1vFHANbnClHtbCtTA3cRk+7WEiB+9gorWphnJ5VzaOLcqDlkA0VIl0Nlq6gF8Hb2QDJetcQgQUwa/kE+FRDYSYm6dRjXRIMhsAu8LLi7TvP96F286onYZcewcrEawtWtCzWIBGCXC92a24w9ryTBM+FFhM62u0XfV7wRR1CR6ORrxfqT71WXYuUFR/vGFlGcSH6M+wbnMRlPcV+CFmrk0AvnLKUBmtN2BNqb/0JMz5SjtBC7HiZDYYseZe+RhRAsUc7riJk6HtYIWmFjZvC5+SZyFji7mzenO/sR9FYLg9V8mFu3HbV0bDg//q//f/D8ld13JQr7qIDhhJ9x3YJQjceHgluQ8mlFCCKgDys6RY1eCrt/Lhx+aejG1nM03cBxZN3ty+fbxX3Z3TkOopfOTCsQqzoghHBwHM3YX14Wo6/qrDJAGl+CZ9sGUv9GPadHInkDnK49IZH/ct3lLcmvOPEZmaywvKxdEFCKvn5loUphH/xK0nJGUJ+Z6FCyBSNG4uITz96ZKKKPj7f9DN6eo75HmhRYXPHyFwr2T4VGYQnlWCloefHGGvBqqQlsNIqc6hmwq/jrE7o5XxPLGcIn4F09WjlIOhtfCMlkkw58toSrROvUyRjZrmvvmN1B4/QOX/6zrr17ct+LH7TRqZPUkPdqcz+rg1yuCam1S9k9wpLev3+0cWxG1vZF7/2j2Q/Gz7KcQrMAbbhFTWdMbPck2mTxFpiUan66w8MikZ6zff7NTUO1kwPVZOat2vrCNrt9QsK/ndN1jS5KupcFxbNES+8k/+u++6xgv7Di5YuohTLy36weTOgflDMjaYDDBYMIhzIKJlJIb6UgJYumZKaWyOKlYnSq0jw6X74geac+vu2Psrnd401Rpe9T9uzboWghG42NHH4p3KbOaY2c/nWKay6TJlJMy6BSHmHBlguPI1pdxmII0jLs4rVgs8jZ+XNohH4KO7dkDOJ9Wkp56ZGtqgUb7um7g08lf3pU6ZRMj1z+stOCMwotL5PNyCJH47S4p7GyX3fQ8OP7ogpF7Kr2lHmFS+PjaqgmZL4b/0NY6hKT6JHRObVCSNRG0sxt1GJf0xLkN8g92HF84Wl/npxAKlj//ytVHsh759daNL22vW5dPmgWtQW1NLhgZnG+3WTgjLpM++OG01n3nZVIjrq/PQ1RWG1j0eZXqYlGByJTiSNdo+cSkrMCwmB20JhoNnN6g0UmFGX9zpambGAnC6izh4hKzPM3V2gmfUTEqlEbJxo0HNuJSFXP3L9l8xZvXMLcWLyXsjYUrx5d7RiVCjgjuqKo8OiX5cV9CKAxC40e1kEaN1ldXM5K4DNssUic3zXz/3vNyJc065TSH04RYjqelfPOvrx18urdPzFQsWlrCEDr62b+veGF+5aPWw30MeYVEvZ+KFTMStUg45HP3w3fIfEqGNTbtfehRhzZxmpIUU7BFKnn3biZtRmu9DtPN8k5Olj+0UEpP9f947N3UIhEpp+NWLhiJZowJJFG9JASdLAJPb31ELI3KTsSlqeVO6JZEIXviS4p8EXkRXT5s9Z/9wPk7e7JKmQ4VKOfFOzJRhw6JwHyQHNp3KBzvEpGeZfSe+CO1j/2uUv/slFeU0aJ2L8Po0NLoFEeKcQgRxxvAB5+bL6sYecLQ0uUUNH2dYpG2f0bL+2qZXYX45fpwpQPKYUvDi5aPZSiUtpWvSKQLKdLSc/To7K5p99gdrv9ebMwN2Ft1nJgpQWvmJB5QjCZPGHQshMPl2sRPkzBd3i6ZxFUYxdLnHPo0SJuu+8Omzdiysf4IxmoQDtZhhN9bWy8S29ONqBjRHVBK7z1nS8nM0mAa10FfvNJgwo0yI1OXp1eJdQXW9FST0cqabW983TYLU5voYJdSL8ISyATUHoi8f8/l/rVL7mnvmL9UGdTpWQgFShhCcJRZIgkFbRaUtOQSety258yuU2lOTeqiv8HaL/94fsIz63AsRTun2ozW7JspcXPV+kaqh2yypCzeyiY9sljBNuSNFf45Jale8UqdCLEjYakGdYuKUxxc8J5ohxG/o+3T5f7ZULLwqwVvKpRQRWpaIottkOMwKtK143DNdy6ISlWbqPrhpGQpo1EWFN29N61twQ1c3TPfzVahwt1nm/zWDqhWppNraU5kbmv9/TVQr5GWOP8drrxv67bHn/cQPwW4J75Nzls9FOqKrHH/qDaT0vvaxtwykQqpt4ycDcQ/EReIyRcqpiuUc4GiEq5RZI/2piYS6MTgLx9vDu8JDUbbdawGQ/Q95mZNqah6KHJaRlgh8+X6G6maFkU1TD2pNCMUbpRvVYjWsjSedSbRpMUIvEhT3SwDD19xaKY3m5Z89PKqJ/rlKOH0MUY1WrBIg6OogTh+K6hikqBsqA/CU4wSWSIFJ7W08D7njrhTHe2PWtrVkHNSYULlEiJSXi5u+EJNiHAwJ6hRq9m8UjHXqhpQ/VIhPAR5NOzw2tHN5XEDH7nOJWWZjCYsETNzOJWnef+9n1+tcA2iiBcRu5f9cQFDUKyyZhVNbbivfmpXScmKJKvVIpdpftPTs6NlFQnm+IQ4LSKCOQTWqwoaP3jHDIRzi9XjrpfID2jRz5MM/mNg/72tHlr0JmcUXCdvbv/xRZqIB0kgNvrQwwfVBRZ22gsfnP62wslhYgleHxuJ0Ax2FSLcK89vbJe1y/y82/LSEpzSISIYpiCEse37/LVMeQ7UDN3fdcqg9f/09u1DWwWeZGTNYZ2BxhFUfDbi35OVEBHGUCkGo4xtNu8lWLXjiegFYHvasG6PNbk6UeAXXfKxIVWBnKWUjBLsnmhMAx4Nou8e/Ujx+478G28UlyIOHqNpWKG8QomU2RlrQFDmjy+3ElbVwmeThJMTMnGCsW0QSnbFtX9YIrQ5M1IAc/lhLtW78l9VKmxHUgZ90aznwxVFZApCtNbuWj54TXtqfKBCD+5XuSMv9kEwxqy84sn4+5RdxxFGUrPIrzDqmRqEQ1kY4WAsK8EXIKifEnT59dm1DIaglIzWDPjudh3ZXv1YsLmKg+3o/0X5waDH04PkloQrl/wrR2b+GfMKC+ONGyx71+x+KsXmcqyJv1OZlaNG9Zkx1/3qvVJxVkLBuF0oZrvZH9OpVASVAknuFpuJopHL1Ej8buBSTR3dXIT7n7wWmEfKcpKgpFrl/HR7RVCPtrVKVC1zNu5z1cxbJmKttScQhWrP2aveE4HxRH7X3TPqzJSKWaKcC8uLWD0sQenKLOxwVXWR37KW05N0zo6ffyxz+o9MxuQVZR3tdieiSVLMUNhqOhY/RGFNDABguf//7i8TBjIZ4NidWkHBtxmTS+yd3akBg28zwORYVbbgMwEO/bMuj4GA7m+97/2l2VNp4T9Jo+BZ4S/2N/1c3RwIAIAEA9T2s5cefKfEvrCvh98VlVkA4nf7la4BAD8C0CMit2jzImUuUucVuhRg0gO4OsxUcrq8jMhbuJyA5Wj/nZv9X/b3NyAAKP3FGX0NLedL2Xm1mdLDPz/LA1MGoZLJYEQaGSI5xxmKyvsMIy6M4eSVwwhpdYyRpPUDo0gHz0RaxzeMpp2LLabycSa2fQn3luCWT0JQO6n8fcA+gyjCM5gkxQzRkBCheuXTDMPlS4aLLyUjNFctI5nqBUYxDcBEbg+B0YRZR4yt3MHE4ecH3yuRubxLuV59FhrQrctsQ3hWUbYyzCBwEqU5qjpH92yhim2xR6eBdVSbdJQ+3XGQKxWjHL5cZxfNzmcrQSOeKwdn4/n04AEtyGd2cCk2ipi/LeTFK9etTuqTeoj6pXjgv6D3o7a/IRAfFy1VdPd0DvzKNZ9UHVOvNLaTV3jFJ2l7f4DzbkpZQH4Q85H48oHOzr7BoYDKR+eYBps0kDzaZdg8kbX1vxuRBvJjSAOq7gSS7PgcLx9fo70udMqTnlsZDvAKiMV5nYGuTj7DLvD5PG/RRpx56c6MdJ0nJKYOLZlB3WKBjRSFBJsEH2j5rNMuCBl1zJquZLzOVLq6Y6uju+0WRvCGkIno8Hp+aZq7ebdeMa96yQ5cDf/P6+UbafiU7Gh2fhY7qGo2sPPkHv0TU2WMk7/wJH6kAAwCYZ98Diofma9dqyR4HwneNB7J6++phgKQPjgcIvLXVsyMiFzFvZTmlRSV1JNsWGk4JTs8GdDrzpKZWg2ladJHN7LWmQ2lrbQxYw5dxUs1Js/hMUMLvO6ON2rPZ3aGgI7wNG7eqRha+aOYzAyKBqiQB4cMsqOepL0xetqhSZUG4FV56Uznv11z3lc07BV3469t+O5puWP93500DDk4FF+RU24G/4+4xiKu25vnh/J5a9X/VwXZwe557wZ4D01VDUCFcVfWALu75v9XuaSUH6LZVqJkFlap0jgIjfCWWbLlyFP4kP/X3M8tAAAA);
        }

        .Scary { 
          font-family: 'scary';
          font-size: ${fontSize};
          vertical-align: ${verticalAlign};
          color: red;
          display: inline-block;
        }

        /* 
          Generate different animation classes for each letter and
          randomise the animation by setting a random place in
          the animation to start it.

          Note that we only make these classes available if the person
          has not expressed a preference for reduced motion to remain
          accessible to people with vestibular motion disorders
          (motion sickness).

          https://vestibular.org/article/coping-support/living-with-a-vestibular-disorder/motion-sickness/
          ).
        */
        @media screen and (prefers-reduced-motion: no-preference) {
          ${
            lettersToAnimate.map(
              (_, index) => `
                .animation${index} {
                  animation: 0.66s jiggle ease -${Math.min(Math.random(), 0.66)}s infinite;
                }
              `
            )
          }
        }

        @keyframes jiggle
        {
          ${
            [...Array(11).keys()]
              .map(i => i*10)
              .map(percentage => kitten.html`
                ${percentage}%
                {
                  transform: translate(${random()}px, ${random()}px) rotate(${random() * 5}deg);
                }
              `)
          }
        }
      </style>
    </span>
  `
}
