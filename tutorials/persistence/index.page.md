---
layout: ../Tutorials.layout.js
imports:
  - import Scary from './Scary.component.js'
title: Persistence
order: 4
description: Learn how to persist information using Kitten’s built-in JavaScript Database (JSDB)
topics:
  - Persistence.
  - JavaScript Database (JSDB).
  - The Kitten CLI’s `db` command.
  - Your project’s entrypoint (the _main.script.js_ file).
  - Database app modules.
---
### Persistence (is not) futile!

So counting kittens is great fun but what happens if you restart the server?

All your kittens are lost! (This is a tragedy.)

So let’s fix that.

(Brace yourself, you’re about to use – _drumroll_ – a <${Scary} fontSize='1.25em' verticalAlign='middle' /> database! _Oooh!_)

### 👻 Using JavaScript Database (JSDB) – a (not so) scary database

> 💡 Kitten has recently been upgraded to use the latest JSDB 5.1.0 with JSDF 3 (JavaScript Data Format version 3). If you were storing custom objects or objects with properties that begin with an underscore (_), you should [check out the JSDB 4 to 5 (JSDF 2 to 3) migration guide](https://codeberg.org/small-tech/jsdb#migrating-from-earlier-versions-of-jsdf).

Update your code to match this:

```js
if (kitten.db.kittens === undefined) kitten.db.kittens = { count: 1 }

export default () => kitten.html`
  <h1>Kitten count<h1>
  <p>${'🐱️'.repeat(kitten.db.kittens.count++)}</p>
`
```

Your page should automatically reload in the browser with the new count.

Now refresh the page a few times, manually stop the server, restart it, and load the page again…

__Wait, what?__

__That’s _it?___

__Seriously?__

Yep, that’s the magic of the integrated JavaScript Database (JSDB) in Kitten.

If you don’t believe me, restart the server and note that all your kittens are still there.

If you _still_ don’t believe me (wow, what a cynic), look in the following folder:

```
~/.local/share/small-tech.org/kitten/data 
```

You should see another folder in it that mirrors the path of your project along with the domain and port you ran it at.

For example, if your project is in:

```
/var/home/aral/kitten-playground
```

And you ran it locally at the default domain and port (which is `localhost` and `443`), then the folder will be named:

```
var.home.aral.kitten-playground.localhost.443
```

Inside this folder (your project’s database folder),  you should see a _kittens.js_ file.

Open it up in a text editor and take a look at it.

It should look something like this:

```js
export const _ = { 'count': 40 };
_['count'] = 41;
_['count'] = 42;
```

That’s what a table looks like in [JavaScript Database (JSDB)](https://codeberg.org/small-tech/jsdb#readme), the database that’s integrated into Kitten and available from all your routes via the global `db` reference.

#### Kitten db command

Kitten provides you with a simple way to get information about and see a live view of your tables.

To get general information about the database, including where the database is located and which tables it has, use the `db` command *from your project’s folder*:

```shell
kitten db
```

If you want to see a live view of the contents of a table, add its name to the `db` command.

So for the Persisted Kitten Count example, if you wanted to see the kittens table, you would enter:

```shell
kitten db kittens
```

> 💡 You are not limited to storing plain objects in your database. You can also store custom objects (instances of custom classes) and get them back with the correct type when you read them. That’s a more advanced feature, however, and you will need to implement a database app module to do it.
>
> Check out the [database app module](https://codeberg.org/domain/app/src/branch/main/app_modules/database/database.js#L198) in the [Domain](https://codeberg.org/domain/app) project to see an example of advanced database use in a larger real-world app.

> 💡 There’s so much more to JSDB and you can learn all about in the [JSDB documentation](https://codeberg.org/small-tech/jsdb#readme).

#### Initialising things

If initialising your database table in your route feels a little yucky, that’s because it is. What if more than one route needed to use that table? If we weren’t absolutely certain that the routes would be called in a given order, we’d have to repeat the conditional initialisation in every route just to be safe.

You can see how this could become a maintenance nightmare.

Fear not, Kitten to the rescue!

If you create a special script called _main.script.js_ in the root of your project folder and export a default function from it, Kitten will import that function and run it at start up.

This is a great place to carry out global initialisation for your app. This function also gets passed a parameter object with a property called `app` that’s a reference to the [Polka](https://github.com/lukeed/polka) app instance so you can perform advanced tasks like adding custom middleware, etc., if you need to.

For our purposes, we could move the conditional initialisation of our database table to _main.script.js_ like this:

```js
export default function () {
 if (kitten.db.kittens === undefined) kitten.db.kittens = { count: 1 }
}
```

Which would leave our route looking rather pristine:

```js
export default () => kitten.html`
  <h1>Kitten count</h1>
  <p>${'🐱️'.repeat(kitten.db.kittens.count++)}</p>
`
```

> 💡 There is another way to achieve this that’s specifically designed for the purpose of initialising databases (and adding strong typing to them) using a special App Module. You haven’t seen App Modules yet but, to get a feel for it, take a look at how it’s used in Domain: https://codeberg.org/domain/app/src/branch/main/app_modules/database

Right, so now that we know that persisting things isn’t scary in Kitten, let’s take another aside and look at one of Kitten’s nifty features: its interactive shell (REPL). And, in the process, we’ll also learn more about Kitten’s JSDB database and take a first look at how components and fragments work in Kitten.

__Next tutorial:__ [Kitten’s interactive shell (REPL)](../kitten-s-interactive-shell-repl)
