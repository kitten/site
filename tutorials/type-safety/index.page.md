---
layout: ../Tutorials.layout.js
title: Type Safety
order: 2
description: Learn how to implement type safety in your Kitten apps.
topics:
  - JSDoc.
  - TypeScript Language Server.
  - The type-safe Kitten namespace package (_@small-web/kitten_).
---
### A little less magic, a little more type

A design goal of Kitten is to be easy to play with. Want to spin up a quick experiment or teach someone the basics of web development? Kitten should make that simple to do. Having magic globals like the `kitten.html` tagged template you saw earlier help with that.

However, for larger or longer-term projects where maintainability becomes an important consideration, you might want to implement type checking. Contrary to popular belief, you don’t have to use TypeScript or have a build process for this. 

You can simply document your types using [JSDoc](https://jsdoc.app/) and turn on type checking by adding a `// @ts-check` comment to the top of your JavaScript files. Any modern development environment that supports language intelligence for JavaScript via the [TypeScript  Language Server](https://www.npmjs.com/package/typescript-language-server) (e.g., [Helix Editor](https://helix-editor.com), [VSCodium](https://vscodium.com/), etc.) will pick them up and use them to provide you with static type checking, auto-completion, etc.

The question is, what do you do about Kitten’s magic globals?

(If you have type checking on, you will get errors like `Cannot find name 'html'` if you use them.)

The answer is, you can install and use the [type-safe Kitten namespace package](https://codeberg.org/kitten/globals) (`@small-web/kitten`) in your project.

Here’s how you’d update [the Kitten Count example](../dynamic-pages) to do this:

1. Create a miminal _package.json_ file:
   ```json
   {
       "name": "kitten-count-typed",
       "type": "module"
   }
   ```

2. Install the package:

   ```shell
   npm install @small-web/kitten
   ```

3. Import the strongly-typed kitten object (it’s the default export):

   ```js
   // @ts-check
   import kitten from '@small-web/kitten'
   
   let /** number */ count = 1
   
   export default () => kitten.html`
     <h1>Kitten count</h1>
     <p>${'🐱️'.repeat(count++)}</p>
   `
   ```

All the objects you find under the `kitten` namespace are also exported separately so, if you want to, you can also import just the objects you want. The following listing is thus equivalent to the one above:

```js
// @ts-check
import { html } from '@small-web/kitten'

let /** number */ count = 1

export default () => html`
 <h1>Kitten count</h1>
 <p>${'🐱️'.repeat(count++)}</p>
`
```

Now that you know to use type safety and the [@small-web/kitten module](https://codeberg.org/kitten/globals) to create more maintainable Small Web apps, let’s take a little detour and look at Kitten’s Markdown support.

__Next tutorial:__ [Markdown](../markdown)
