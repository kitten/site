---
layout: ../Tutorials.layout.js
title: Dynamic Pages
order: 1
description: This tutorial kicks off the Kitten Count series of tutorials (tutorials 2-9) that take you through the basics of Kitten by building and evolving an interactive page to count kittens.
topics:
  - Dynamic pages (_.page.js_ files).
  - File system-based routing.
  - The `kitten.html` tagged template string.
  - Security and string sanitisation.
  - The `<page>` tag.
---
You know what’s better than one kitten? Many kittens!

If you were following along from [the previous tutorial](../static-html), rename your _index.html_ file to _index.page.js_ (and otherwise create a new file called _index.page.js_) and update its contents to match the following:

```js
let count = 1

export default () => kitten.html`
  <h1>Kitten count</h1>
  <p>${'🐱️'.repeat(count++)}</p>
`
```

Now run `kitten`, go to _https://localhost_, and refresh the page to see the number of kittens increase each time you do.

✨ Ooh, magic! ✨

> #### How does it work?
> 
> A page in Kitten is written in plain old JavaScript (and hence the _page.js_ extension).
> 
> Kitten uses file system routing. So, in this example, your _/index.page.js_ file is mapped to the root route (_/_) of your site. 
>
> 💡 Since Kitten pages – and components, etc., as you’ll see later – are just JavaScript files, you don’t need any extra tooling to make things with Kitten. Your editor of choice will likely already know how to work with JavaScript (and HTML and CSS) files or can be easily configured to do so.
> 
> A route in Kitten is just an exported default function inside of a JavaScript module. Any HTML you return from your route is sent to the browser.
> 
> You write your HTML inside of `kitten.html` tagged template literals. These are just standard [JavaScript template literals (template strings)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals) that get passed through a special global function called `kitten.html` that performs Kitten-specific magic.
> 
> 🔒 __Important security note:__ Part of that magic relates to the security of your apps. Kitten’s `kitten.html` tagged template literal sanitises any content you interpolate into your templates by default. This is important in avoiding [cross-site-scripting (XSS) attacks](https://owasp.org/www-community/attacks/xss/). Kitten does the right thing by default when you use `kitten.html` but [if you forget and return a snippet of HTML in a plain JavaScript string, for example](https://owasp.org/www-community/attacks/xss/), you will not be protected.

The example above contains the minimum code required to achieve our goal. However, here’s a more explicit version in case you’re not familiar with JavaScript arrow functions. In the version below, you can explicitly see everything that’s happening.

```js
let count = 1

export default function route ({ request, response }) {
  return kitten.html`
    <h1>Kitten count</h1>
    <p>${'🐱️'.repeat(count++)}</p>
  `
}
```

With the above version of the code, it’s clear that what you’re exporting from the _.page.js_ file is a route and that it gets passed a parameter object containing the HTTP request and response objects (which we are not using in this example).

Note that routes in Kitten take a parameter object (notice they are surrounded by `{ … }`) instead of positional parameters which you might be familiar with from more traditional frameworks like Polka, Express, etc. Parameter objects make it easier to read code and are also used elsewhere in Kitten (components and fragments, the `onConnect()` handler, etc.) 

### Demystifying things

Take a look at the title of the tab when you run the example. Do you see that it reads “Kitten count”?

That’s odd, you didn’t tell Kitten to set the title of the page but if you look at the source code of the page, you’ll see that it’s set in the `<head>` of your page as:

```html
<title>Kitten count</title>
```

How’d that happen?

Well, Kitten tries to do the right thing by default. So, if you don’t set a title for your page, it tries to derive one for you by looking to see if there is a `<h1>` tag on your page and using that if there is. It might not be perfect but it’s better than nothing if all you’re doing is coding up a quick example.

But what if you want to set your own title?

Enter, the `page` tag.

### The `<page>` tag

Kitten has a special tag called `<page>` that you can use to set commonly found elements in the heads of your pages (`lang`, `title`, `charset`, `icon`, and `viewport`) as well as to include the libraries that Kitten has first-class support for ([HTMX](https://htmx.org/), [HTMX idiomorph extension](https://github.com/bigskysoftware/idiomorph?tab=readme-ov-file#htmx), [HTMX WebSocket extension](https://github.com/bigskysoftware/htmx-extensions/tree/main/src/ws), [Alpine.js](https://alpinejs.dev), and [Water.css](https://watercss.kognise.dev/)).

To use it, you simply include the tag anywhere on a page (or fragment or component):

```js
let count = 1

export default () => kitten.html`
  <page title='Kitten count example'>

  <h1>Kitten count</h1>
  <p>${'🐱️'.repeat(count++)}</p>
`
```

> 💡 You can also use the `<page>` tag to add classes to the body tag. e.g.,
>
> ```js
> <page bodyClass='someClass someOtherClass'>
> ```

If you run the example now, you’ll see that the title you supplied is shown instead of the default one derived from the `<h1>` element.

> 💡 In addition to the `<html>` tag’s `lang` attribute, the regular `<head>` settings, and supported libraries, you can also use the `syntax-highlighter` attribute to automatically use the default highlight.js light and dark mode themes (the WCAG AA accessible [Measured light and dark themes](https://measured.co/blog/accessible-syntax-highlighting-colour-schemes-for-developers)) for code blocks rendered from `<markdown>…</markdown>` sections in your code. If you want to choose your own theme instead, you can override default themes by manually adding any of the [roughly 500 highlight.js themes that exist](https://highlightjs.org/examples) to your project and specify the relative or absolute URL to them in the `syntax-highlighter-theme-light` and `syntax-highlighter-theme-dark` attributes.

> 💡 You’ll see later that you can set anything you want in the head of your pages using the special `HEAD` slot (`<content for='HEAD'>…</content>`) but the `<page>` tag should let you set the most common things you need when making Small Web sites and apps with Kitten.

As you can see, Kitten is very easy to get started with. But if you’re planning on creating an application that you see yourself supporting for a long time, you might be wondering about maintainability. Specifically, what if we want to make our code easier to write and debug by implementing type safety?

Kitten has you covered… 

__Next tutorial:__ [Type Safety](../type-safety)

