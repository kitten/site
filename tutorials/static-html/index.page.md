---
layout: ../Tutorials.layout.js
title: Static HTML
order: 0
description: Make your first static “Hello, world!” Kitten page using plain old HTML.
topics:
   - How to run Kitten.
   - The basics of how to use the Kitten server in development.
   - How to create a static HTML page.
---
Let’s quickly create and test your first “Hello, world!” Kitten site.

1. Create a directory for the example and enter it:
   
   ```shell
   mkdir kitten-playground
   cd kitten-playground
   ```

2. Create a file called _index.html_ and add the following content to it:
   
   ```html
   <h1>Kitten</h1>
   <p>🐱️</p>
   ```

3. Run Kitten using the following syntax:
   
   ```shell
   kitten
   ```

Once Kitten is running, hit *[https://localhost](https://localhost)*, and you should see your new site.

> 💡 When you run `kitten` without any arguments, it defaults to serving the current directory. You can also specify a path as the first argument (`kitten [path to serve]`) to serve a different directory than the one you’re currently in.
> 
> 💡 Notice that you didn’t get a certificate error. This is because Kitten automatically creates a development-time TLS certificate for your local machine signed by a local certificate authority that it adds to your system’s trust store (and to Firefox, if you have it installed) using [Auto Encrypt Localhost](https://codeberg.org/small-tech/auto-encrypt-localhost).
> 
> (If you’re running on Windows under WSL 2 and you do get a certificate error, please remember to [manually install the local development certificates in your browsers](#you-must-manually-install-the-certificate-authority-in-your-windows-browsers).)
> 
> 💡 You’re not limited to accessing your local machine via _https://localhost_. You can also use any local IP address that it’s accessible from (see [Accessing your local machine from other devices on your local area network](https://codeberg.org/small-tech/auto-encrypt-localhost#accessing-your-local-machine-from-other-devices-on-your-local-area-network) for more information). You can also use the IP aliases 127.0.0.2 - 127.0.0.4 and localhost aliases (subdomains) place1.localhost - place4.localhost without certificate errors. The latter, especially, are useful when testing the peer-to-peer features of Small Web apps.
> 
> 💡 Small Web places made with Kitten are meant to be owned and used by one person. Until an identity and secret has been generated for this person, Kitten will display a link to the special page that generates them. (Your secret on a Small Web place never hits the server.) You can also generate this secret from [Kitten’s REPL](/tutorials/kitten-s-interactive-shell-repl) using the `.id create` command.)
> 
> 💡 By default, Kitten will be as quiet as possible and only surface warnings and errors. If you want more extensive logging, start it with the VERBOSE environment variable set:
> 
> `VERBOSE=true kitten [path to serve]`
> 
> Similarly, if you want to see performance statistics, set `PROFILE=true`.
> 
> Note that the first time Kitten is run, it will create a TLS certificate authority and add it to your system trust stores as well as to Firefox and Chrome so you can run your development environment from [https://localhost](https://localhost) to match the deployment environment as closely as possible. Your operating system will ask your permission before allowing this.

So we’ve seen how Kitten happily serves any HTML you throw at it just like any good web server should.

But you can render HTML using any old web server…

Let’s start doing things no other web server can do, shall we?

__Next tutorial:__ [Dynamic Pages](../dynamic-pages)
