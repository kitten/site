import fs from 'node:fs'
import path from 'node:path'
import { parse } from 'yaml'
import { html } from '@small-web/kitten'
import Tutorials from './Tutorials.layout.js'

// Create index using the front matter of tutorial Markdown pages.
// As this occurs once on initial server start, we can use synchronous methods.
const indexHtml = []
fs.readdirSync('./tutorials', { withFileTypes: true })
  .filter(file => file.isDirectory())
  .forEach((file) => {
    const filePath = path.join(path.resolve(), 'tutorials', `${file.name}/index.page.md`)
    const markdown = fs.readFileSync(filePath, 'utf-8')
    const frontMatter = extractYamlFrontMatter(markdown)
    const slug = kitten.slugify(frontMatter.title)

    const html = kitten.html`
      <section id='${slug}'>
        <h2><a href='${slug}'>${frontMatter.order + 1}. ${frontMatter.title}</a></h2>

        <p>${[kitten.md.render(frontMatter.description)]}</p>

        <h3>Topics covered</h3>
        <ul>
          ${frontMatter.topics.map(topic => kitten.html`<li>${[kitten.md.renderInline(topic)]}</li>`)}
        </ul>
      </section>
    `
    indexHtml[frontMatter.order] = html
  })

/** Index fragment. Displays tutorials index. */
const Index = () => indexHtml

export default () => html`
  <${Tutorials} isIndex>
    <markdown>
      ## Tutorials

      These tutorials teach you how to build Small Web apps using Kitten.

      They start from the basics of traditional web development, using HTML, CSS, and JavaScript and proceed to cover the unique features available only in Kitten like the Streaming HTML workflow and building peer-to-peer web apps (also known as Small Web apps).

      [Get started.](static-html)
    </markdown>

    <${Index} />
  </>
`

/**
  Extract and parse YAML front matter from Markdown.

  @param {string} markdown
*/
function extractYamlFrontMatter (markdown) {
  let boundaryCount = 0
  return parse(markdown.split('\n').filter(line => {
    if (line === '---') {
      boundaryCount++
      return false
    }
    return (boundaryCount <= 1) 
  }).join('\n'))
}
