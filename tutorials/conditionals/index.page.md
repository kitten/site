---
layout: ../Tutorials.layout.js
title: Conditionals
order: 6
description: Learn how to implement conditional statements in Kitten templates.
topics:
  - The different ways to implement conditional logic in Kitten templates.
  - Kitten’s `<if ${condition}><then>…</then><else>…</else></if>` tags.
---
### How many kittens are too many kittens?

So kittens are great but maybe after a certain number we should truncate the kitten emojis and display the exact count to save the person from having to count them, as fun as that might be.

Enter, conditionals.

Let’s add a conditional so once there are more than twenty kittens, we show a message that states the additional number instead of showing yet more kitten emojis:

```js
if (kitten.db.kittens === undefined) kitten.db.kittens = { count: 1 }

export default () => {
  kitten.db.kittens.count++

  return kitten.html`
    <h1>Kitten count</h1>
    <p>${'🐱️'.repeat(kitten.db.kittens.count > 20 ? 20 : kitten.db.kittens.count)}</p>

    <if ${kitten.db.kittens.count > 20}>
      <p>(and ${kitten.db.kittens.count - 20} more.)</p>
    </if>
  `
}
```

Ooh, what’s that? An `<if>` tag? That’s not HTML.

No, it’s not. It’s a little extension to HTML that’s unique to Kitten.

What you see above is actually a convenient shorthand form of the `<if>` tag that comes with an important limitation: it can only be used to display one tag.

In the above example, that means that we can only include that one `<p>` tag after the `<if>`. The `<p>` tag itself can have any number of children but you cannot add any sibling tags.

To understand why, let’s look at the full syntax if the `<if>` tag:

```js
kitten.html`
  <if ${condition}>
    <then>
      <h1>The condition is true :)</h1>
      <img src='woot.gif' alt='Geeks partying'>
      <p>And there was much rejoicing!</p>
    </then>
    <else>
      <h1>The condition is false :(</h1>
      <p>Sad trombone.</p>
    </else>
  </if>
`
```

The `<then>` and `<else>` tags, apart from being optional (as you saw in the shorthand version, where they’re omitted), are also _optionally-closed_.

So the above example can also be written as shown below without losing the ability to include any number of siblings:

```js
<if ${condition}>
  <then>
    <h1>The condition is true :)</h1>
    <img src='woot.gif' alt='Geeks partying'>
    <p>And there was much rejoicing!</p>
  <else>
    <h1>The condition is false :(</h1>
    <p>Sad trombone.</p>
</if>
```

In fact, most times, this last form is the one you should favour unless you only have a tiny snippet of code (with a single root node) that you want to add conditionally to your page, in which case you can use the first form you saw in the initial example.

The one downside of this canonical form is that you’re left with a dangling indent right before the closing tag (`</if>`). If that gets on your nerves or makes it more difficult to ensure that your markup is valid at a glance, please feel free to use the full form. It really is up to you.

> 💡 Since the `<then>` and `<else>` tags are optional, the way the parser works is that it takes the first child of the `<if>` tag as the `true` condition and the second child as the `false` condition.

> 🪤 The `<if>` conditional does not short circuit so if you have values in your branches that might be null or undefined, use conditional chaining to ensure you don’t encounter runtime errors. Either that, or use JavaScript conditionals in your templates instead.

Now that you know how to implement conditional logic in your Kitten templates, let’s learn about how we can persist a different Kitten count for different browser sessions.

__Next tutorial:__ [Sessions](../sessions)
