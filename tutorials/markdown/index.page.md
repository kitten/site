---
layout: ../Tutorials.layout.js
title: Markdown 
order: 3
description: Recreate the Kitten count example in Markdown.
topics:
  - Kitten’s Markdown support.
---
### HTML is so 1991 (introducing Markdown)

HTML is great but did you know you can also use Markdown? (Well, now you do.)

Here’s how [the Kitten Count example](../dynamic-pages) looks in Markdown.

```js
let count = 1

export default () => kitten.markdown`
  # Kitten count
  ${'🐱️'.repeat(count++)}
`
```

Pretty neat!

(Granted, it’s not a great use of Markdown but you get the idea.)

Kitten’s Markdown support comes from [markdown-it](https://github.com/markdown-it/markdown-it#markdown-it-).

Kitten supports a very rich set of Markdown features including [automatic anchors for headings](https://github.com/valeriangalliat/markdown-it-anchor#markdown-it-anchor-), [syntax highlighting](https://github.com/valeriangalliat/markdown-it-highlightjs#readme), [figure (and figure caption)](https://github.com/Antonio-Laguna/markdown-it-image-figures#markdown-it-image-figures) support, [footnotes](https://github.com/markdown-it/markdown-it-footnote#readme), typographical niceties (like converting typewriter quotes "" into curly quotes “”, [insert](https://github.com/markdown-it/markdown-it-ins#readme), [mark](https://github.com/markdown-it/markdown-it-mark#readme), [subscript](https://github.com/markdown-it/markdown-it-sub#markdown-it-sub), [superscript](https://github.com/markdown-it/markdown-it-sup#markdown-it-sup), etc.) and even [table of contents creation](https://github.com/nagaozen/markdown-it-toc-done-right#readme) for Markdown sections.

We’ll look at Markdown in more depth in its own example later. For now, know that you can add Markdown to your pages by using `kitten.markdown` tagged template strings and/or by enclosing Markdown regions within your HTML between `<markdown>…</markdown>` tags.

Additionally, as you’ll see later, you can put dynamic Markdown fragments in their own _.fragment.md_ files. You can also use Markdown pages (_.page.md_ files). In these Markdown pages you can specify front matter in YAML format to set a layout template using the special `layout` property. You can also import components and fragments and use them in your Markdown pages just as you would in regular Kitten pages (with the current limitation that they cannot contain child content of their own – so no slots for the time being). You import components and fragments by specifying their import statements (in the form `import ComponentName from '../path/to/ComponentName.component.js` or `import FragmentName from '../path/to/FragmentName.fragment.js'`) in the `imports` list in your front matter and then using them in your Markdown page as you would normally (e.g., `<${ComponentName} prop=someValue anotherProp=someOtherValue />`). Finally, you can also set custom props that are passed to your layout template. 

> 🚧 The Markdown example is yet to be written but you can see almost all the features demonstrated in examples/components and examples/markdown for the time being.

> 💡 Unlike HTML, Markdown is whitespace-sensitive so make sure you indent your code properly or it will get parsed incorrectly.

Right, our little detour into Markdown is complete so let’s get back to building on our Kitten Count example by persisting the count so that it survives server restarts.

__Next tutorial:__ [Persistence](../persistence)
