import MainLayout from '../Main.layout.js'

export default ({ isIndex = false, title, pageId, order = 0, description = '', topics = '', SLOT }) => {
  let topicsArray = topics.split(',')
  return kitten.html`
    <${MainLayout} class='tutorials' pageId='tutorials'>
      <if ${isIndex}>
        <if ${title !== undefined}>
          <h2>${title}</h2>
        </if>
      <else>
        <h2 class='breadcrumb'><a href='../#${kitten.slugify(title || '')}'>Index</a> <span>›</span> ${Number(order) + 1}. ${title}</h2>
        <div class='overview'>
          ${[kitten.md.render(description)]}

          <h3>Topics covered</h3>
          <ul>
            ${topicsArray.map(topic => kitten.html`<li>${[kitten.md.renderInline(topic)]}</li>`)}
          </ul>
        </div>
      </if>
      ${SLOT}
      <style>
        .tutorials {
          h2 { font-size: 1.25em; }
          .breadcrumb span { font-weight: normal; color: var(--selection); }
          .overview {
            background-color: var(--background-alt);
            padding: 1em;
            border-radius: 1em;
          }
        }
      </style>
    </>
  `
}
